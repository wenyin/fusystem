# fufile

This is EFIT-interface python package repository. Meanwhile, efitpy is an extension for fucore to easily extract data and information from EFIT special format, which is hard to understand literally due to lack of organization and somehow outdated.

这是 EFIT 接口的 Python 包，同时 efitpy 还将作为 fucore 的插件之一来帮助从 EFIT 的特殊格式数据中提取数据和信息，EFIT 格式由于没有妥当的组织，字面上很难理解，另外它也相当过时了。

Simply run the code below would give you a simple demo on how to parse the EFIT file and display a plasma equilibrium.

运行以下代码你就可以看到 EFIT 的 `g` 数据被解析后得到的等离子体平衡了。

```python
python example.py
```

Make sure you have the basic python packages: `numpy`, `matplotlib`, `scipy`, otherwise you may be warned some packages not found.

运行前你得有基本的 Python 包： `numpy`, `matplotlib`,`scipy`, 否则你会看到你缺失了一些包，some packages not found。

![Example Plot for g073999.030400](data/example.png)
Example Plot for g073999.030400

## Prequisite Package

- **Basics:** numpy, scipy, matplotlib, jupyter, notebook, jupyterlab

- **Progressbar**: progressbar2

- **GPU Acceleration:** (optional) cupy, cupyimg
```pip3 install cupy-cuda111 # would works for CUDA-11.1```

  If you encounter the following [Warning](https://stackoverflow.com/questions/56656777/userwarning-matplotlib-is-currently-using-agg-which-is-a-non-gui-backend-so) in Linux system while drawing a plot by matplotlib, 

>  UserWarning: Matplotlib is currently using agg, which is a non-GUI backend, so cannot show the figure.

just install the python3-tk package.

```bash
sudo apt install python3-tk
```

## For Developers
### Package

Make sure you have the latest `setuptools` and `wheel` package installed. 

```bash
python3 -m pip install --user --upgrade setuptools wheel # remove --user if in virtual environment
python3 setup.py sdist bdist_wheel # do this in fusystem package root path
```
