# This function plot the heatmap distribution for the input array. 
# Please input field_Bsurf as a 2D array with shape (number of essential poloidal angle theta^*, number of toroidal angle phi) i.e. (ntheta, nPhi) 
# or a 3D array with shape (number of B surface number, number of essential poloidal angle theta^*, number of toroidal angle phi) i.e. (ns, ntheta, nPhi)
# surf_inds as the list of chosen surface.
# WARNING: the function, at least now, can only handle those situations with less than 10 layers of magnetic surfaces.
# Choose the surface that are representative.
# For developers:
#     This function has not yet been polished enough to achieve a fast plotting speed,
#     for those who are interested in improving it, please refer [Intro to Animations in Python](https://plotly.com/python/animations/) 
# Example:
#     import RMP_visual
#     ind = 160
#     RMP_visual.BSurfHeatmapPlotly(Br_Bsurf[ind], field_name="B^r", S=S[ind], Q=Q[ind])
#     RMP_visual.BSurfHeatmapPlotly(Br_Bsurf, surf_inds=[10, 20, 30, 40, 50, 60], field_name="B^r", S=S, Q=Q)
from numpy import ndarray as _ndarray
# TODO: the heatmap plot is very slow if you are working on a remote server with a low bandwidth, suggested network should be more than 5MB/s

# Some possible use cases:
# fig = RMP_visual.field_at_Bsurf_heatmap_plotly(B1_Bsurf[i_surf], field_name="B^1", S=S[i_surf], Q=Q[i_surf])
# fig = RMP_visual.field_at_Bsurf_heatmap_plotly(B1_Bsurf, surf_inds=[200,210,220,230],  field_name="B^1", S=S, Q=Q)
# fig = RMP_visual.field_at_Bsurf_heatmap_plotly(np.roll(B1_Bsurf[i_surf], 20, axis=1), field_name="B^1", S=S[i_surf], Q=Q[i_surf])
# fig.show()
def field_at_Bsurf_heatmap_plotly(field_Bsurf:_ndarray, surf_inds:list=None, field_name='Something', S=None,  Q=None):
    import numpy as np
    import plotly.graph_objects as go

    if field_Bsurf.ndim==3:
        assert(len(surf_inds)>1)
    elif field_Bsurf.ndim==2:
        assert(surf_inds is None)
    
    # Create figure
    fig = go.Figure()
    if field_Bsurf.ndim==3:
        ns, ntheta, nPhi = field_Bsurf.shape
    elif field_Bsurf.ndim==2:
        ntheta, nPhi = field_Bsurf.shape
    else:
        raise RuntimeError("Please input field_Bsurf as a 2D array with shape (number of essential poloidal angle theta^*, number of toroidal angle phi) i.e. (ntheta, nPhi) or a 3D array with shape (number of B surface number, number of essential poloidal angle theta^*, number of toroidal angle phi) i.e. (ns, ntheta, nPhi)")
    
    # Shift the distribution from [0,2pi] to [-pi,pi]
    TET = np.linspace(0, 2*np.pi, ntheta)
    pi_ind = np.argmin(np.abs(TET-np.pi))
    field_Bsurf_TETsym = []
    if S is not None: S_list = []
    if Q is not None: Q_list = []
    surf_num = 0
    if field_Bsurf.ndim==3:
        for s in surf_inds:
            field_Bsurf_TETsym.append(np.roll(field_Bsurf[s], pi_ind, axis=0))
            if S is not None: S_list.append(S[s])
            if Q is not None: Q_list.append(Q[s])
            surf_num += 1
    elif field_Bsurf.ndim==2:
        field_Bsurf_TETsym.append(np.roll(field_Bsurf, pi_ind, axis=0))
        if S is not None: S_list.append(S)
        if Q is not None: Q_list.append(Q)
        surf_num += 1
    Phi = np.arange(0, 2*np.pi, 2*np.pi/nPhi)
    TET = np.linspace(-np.pi, np.pi, ntheta)
        
    # Hearmap Config
    contours = dict(coloring ='heatmap',
                    showlabels = True, # show labels on contours
                    labelfont = dict(size = 12, color = 'white',)) # labels marking the contour isolines font 
    colorbar = dict(title=r' ${{{field_name}}}\text{{ (T)}}$'.format(field_name=field_name),
                    titleside='top', titlefont=dict(size=10, family='Arial, sans-serif')
                    )
    visible = False if surf_num>1 else True
    if field_Bsurf.ndim==3:
        titletext = f"{field_name} distribution on some magnetic surfaces"
    elif field_Bsurf.ndim==2:
        # TODO: Trouble in showing the field name in latex format.
        if Q is None and S is None: 
            titletext = r"${{{field_name}}}\text{{ distribution on the magnetic surface}}$".format(field_name=field_name)
        elif Q is None and S is not None:
            titletext = r"${{{field_name}}}\text{{ distribution on the magnetic surface @ }}s={{{s:.4f}}}$".format(field_name=field_name, s=S_list[0])
        elif Q is not None and S is None:
            titletext = r"${{{field_name}}}\text{{ distribution on the magnetic surface @ }}q={{{q:.3f}}}$".format(field_name=field_name, q=Q_list[0])
        elif Q is not None and S is not None:
            titletext = r"${{{field_name}}}\text{{ distribution on the magnetic surface @ }}s={{{s:.4f}}}, q={{{q:.3f}}}$".format(field_name=field_name, s=S_list[0], q=Q_list[0])
    title={
        'text': titletext,
        'y':0.9, 'x':0.5,
        'xanchor': 'center',
        'yanchor': 'top'
    }
    
    # Add traces, one for each slider step
    for s in range(surf_num):
        if Q is None and S is None: 
            name = f"Unknown Surface" 
        elif Q is None and S is not None:
            name = f"s = {S_list[s]:.4f}" 
        elif Q is not None and S is None:
            name = f"q = {Q_list[s]:.3f}" 
        elif Q is not None and S is not None:
            name = f"s = {S_list[s]:.4f},q={Q_list[s]:.3f}" 
        fig.add_trace(
            go.Contour(
                z=field_Bsurf_TETsym[s], 
                y=TET, x=Phi, 
                visible=visible,
                name=name,
                contours=contours,
                colorbar=colorbar)
        )
        
    if field_Bsurf.ndim==3:
        fig.data[0].visible = True # Make 1st trace visible
        steps = [] # Create and add slider
        for i in range(len(fig.data)):
            step = dict(
                method="restyle",
                args=["visible", [False] * len(fig.data)],
            )
            step["args"][1][i] = True  # Toggle i'th trace to "visible"
            steps.append(step)
        sliders = [dict(
            active=0,
            steps=steps
        )]
        fig.update_layout(sliders=sliders)
    elif field_Bsurf.ndim==2:
        if Q is not None:
            y0 = 0
            for i_fl in range(10):
                y1 = y0+2*np.pi / Q_list[0]
                fig.add_trace(go.Scatter(x=[0, 2*np.pi], y=[y0, y1], mode='lines', showlegend=False, line = dict(color='royalblue', width=1.5, dash='dot')))
                if y1 > np.pi:
                    y1 -= 2*np.pi
                    y0_back = y1 - 2*np.pi / Q_list[0]
                    fig.add_trace(go.Scatter(x=[0, 2*np.pi], y=[y0_back, y1], mode='lines', showlegend=False, line = dict(color='royalblue', width=1.5, dash='dot')))
                if y1 <-np.pi:
                    y1 += 2*np.pi
                    y0_back = y1 - 2*np.pi / Q_list[0]
                    fig.add_trace(go.Scatter(x=[0, 2*np.pi], y=[y0_back, y1], mode='lines', showlegend=False, line = dict(color='royalblue', width=1.5, dash='dot')))
                y0 = y1
    
    fig.update_layout(
        title=title,
        xaxis = dict(range=[0, 2*np.pi], title=r'$\varphi \text{(rad)}$'),
        yaxis = dict(range=[-np.pi, np.pi], title=r'$\theta^* \text{(rad)}$')
    )
    return fig


def field_at_Bsurf_heatmap_matplotlib(field_Bsurf:_ndarray, surf_inds:list=None, field_name='Something', S=None,  Q=None):
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.animation as animation
    
    if field_Bsurf.ndim==3:
        assert(len(surf_inds)>1)
        raise RuntimeError("The function has not yet prepared for multi-surfaces animation because some technical problems. Use single surface mode please.")
    elif field_Bsurf.ndim==2:
        assert(surf_inds is None)

    # Create figure
    fig, axs = plt.subplots(1, 1, figsize=(12.0,5.0))
    if field_Bsurf.ndim==3:
        _, ntheta, nPhi = field_Bsurf.shape
    elif field_Bsurf.ndim==2:
        ntheta, nPhi = field_Bsurf.shape
    else:
        raise RuntimeError("Please input field_Bsurf as a 2D array with shape (number of essential poloidal angle theta^*, number of toroidal angle phi) i.e. (ntheta, nPhi) or a 3D array with shape (number of B surface number, number of essential poloidal angle theta^*, number of toroidal angle phi) i.e. (ns, ntheta, nPhi)")
    
    # Shift the distribution from [0,2pi] to [-pi,pi]
    TET = np.linspace(0, 2*np.pi, ntheta)
    pi_ind = np.argmin(np.abs(TET-np.pi))
    field_Bsurf_TETsym = []
    if S is not None: S_list = []
    if Q is not None: Q_list = []
    surf_num = 0
    if field_Bsurf.ndim==3:
        for s in surf_inds:
            field_Bsurf_TETsym.append(np.roll(field_Bsurf[s], pi_ind, axis=0))
            if S is not None: S_list.append(S[s])
            if Q is not None: Q_list.append(Q[s])
            surf_num += 1
    elif field_Bsurf.ndim==2:
        field_Bsurf_TETsym.append(np.roll(field_Bsurf, pi_ind, axis=0))
        if S is not None: S_list.append(S)
        if Q is not None: Q_list.append(Q)
        surf_num += 1
    Phi = np.arange(0, 2*np.pi, 2*np.pi/nPhi)
    TET = np.linspace(-np.pi, np.pi, ntheta)
    
    # pcolormesh! 
    if field_Bsurf.ndim==2:
        p = axs.pcolormesh(
                Phi[None,:]*np.ones((ntheta, nPhi)), 
                TET[:,None]*np.ones((ntheta, nPhi)), 
                field_Bsurf_TETsym[0], cmap='hot', shading='gouraud')
        fig.colorbar(p, ax=axs)
    elif field_Bsurf.ndim==3:
        imgs = []
        for s in range(surf_num):
            imgs.append(plt.pcolormesh(
                Phi[None,:]*np.ones((ntheta, nPhi)), 
                TET[:,None]*np.ones((ntheta, nPhi)), 
                field_Bsurf_TETsym[s], cmap='hot', shading='gouraud'))
        ani = animation.ArtistAnimation(fig, imgs, interval=50, repeat_delay=3000, blit=True)

        # for s in range(surf_num):
        #     if Q is None and S is None: 
        #         name = f"Unknown Surface" 
        #     elif Q is None and S is not None:
        #         name = f"s = {S_list[s]:.4f}" 
        #     elif Q is not None and S is None:
        #         name = f"q = {Q_list[s]:.3f}" 
        #     elif Q is not None and S is not None:
        #         name = f"s = {S_list[s]:.4f},\n q={Q_list[s]:.3f}" 
        #     fig.add_trace(
        #         go.Contour(
        #             z=field_Bsurf_TETsym[s], 
        #             y=TET, x=Phi, 
        #             visible=visible,
        #             name=name,
        #             contours=contours,
        #             colorbar=colorbar)
        #     )
        
        
    # if field_Bsurf.ndim==2:
    #     if Q is not None:
    #         y0 = 0
    #         for i_fl in range(10):
    #             y1 = y0+2*np.pi / Q_list[0]
    #             fig.add_trace(go.Scatter(x=[0, 2*np.pi], y=[y0, y1], mode='lines', showlegend=False, line = dict(color='royalblue', width=1.5, dash='dot')))
    #             if y1 > np.pi:
    #                 y1 -= 2*np.pi
    #                 y0_back = y1 - 2*np.pi / Q_list[0]
    #                 fig.add_trace(go.Scatter(x=[0, 2*np.pi], y=[y0_back, y1], mode='lines', showlegend=False, line = dict(color='royalblue', width=1.5, dash='dot')))
    #             if y1 <-np.pi:
    #                 y1 += 2*np.pi
    #                 y0_back = y1 - 2*np.pi / Q_list[0]
    #                 fig.add_trace(go.Scatter(x=[0, 2*np.pi], y=[y0_back, y1], mode='lines', showlegend=False, line = dict(color='royalblue', width=1.5, dash='dot')))
    #             y0 = y1

    axs.set_xlabel('$\phi$', fontsize=15)
    axs.set_ylabel(r'$\theta^*$ (rad)', fontsize=15)
    if field_Bsurf.ndim==3:
        titletext = f"{field_name} distribution on some magnetic surfaces"
    elif field_Bsurf.ndim==2:
        if Q is None and S is None: 
            titletext = f"{field_name} distribution on the magnetic surface" 
        elif Q is None and S is not None:
            titletext = f"{field_name} distribution on the magnetic surface @ s={S_list[0]:.4f}" 
        elif Q is not None and S is None:
            titletext = f"{field_name} distribution on the magnetic surface @ q={Q_list[0]:.3f}" 
        elif Q is not None and S is not None:
            titletext = f"{field_name} distribution on the magnetic surface @ s={S_list[0]:.4f}, q={Q_list[0]:.3f}" 
    axs.set_title(titletext, fontsize=14)
    axs.set_xlim([0.0, 2*np.pi])
    axs.set_ylim([-np.pi, np.pi])
    
    # To save this second animation with some metadata, use the following command:
    # im_ani.save('im.mp4', metadata={'artist':'Guido'})
    if field_Bsurf.ndim==2:
        return fig, axs
    elif field_Bsurf.ndim==3:
        return fig, axs, imgs, ani

# sm spectrum 
def b1_sm_spectrum_nfixed(b1_nm_bis, S, mode:str, n_har:int, mplot_half=20, Q:_ndarray=None):
    # adapted from 'spectrum_plot.py'
    import matplotlib.pyplot as plt
    import numpy as np
    ns, nm = b1_nm_bis.shape
    m_half = (nm-1)//2 # designed for odd number but also work for even case
    assert(mode=='abs' or mode=='angle')

    if mode=='abs':
        b1_eff = np.zeros(( ns, 2*m_half+1 ))
    elif mode=='angle':
        from colorspacious import cspace_convert
        from matplotlib.colors import LinearSegmentedColormap
        # There is an color setup data file "erdc_iceFire.txt" which make the angle color looks much better.  
        data = np.loadtxt("erdc_iceFire.txt")
        data2 = np.clip(cspace_convert(data, "sRGB1", "sRGB1"), 0, 1)
        cm = LinearSegmentedColormap.from_list('erdc_iceFire', data2)
        b1_angle = np.zeros(( ns, 2*m_half+1 ))
    m_coord = np.zeros(( ns, 2*m_half+1 ))
    S_coord = np.zeros(( ns, 2*m_half+1 ))
    for m in range(-m_half, m_half+1):
        m_index = m + m_half
        for s in range(ns):
            if mode=='abs': b1_eff[s, m_index] = np.abs(b1_nm_bis[s, m])
            elif mode=='angle': b1_angle[s, m_index] = np.angle(b1_nm_bis[s, m])
            m_coord[s, m_index] = m # TODO: have not yet checked correctness for both even nm and odd nm cases. Seems right now. 
            S_coord[s, m_index] = S[s]

    if (mplot_half>m_half):
        raise RuntimeWarning("Requested mplot_half is too large to plot, fix it by hand")

    fig, axs = plt.subplots(1, 1,  figsize=(7.5,5.0))
    if mode=='abs': c = axs.pcolormesh(
        m_coord[:, m_half-mplot_half:m_half+mplot_half+1], 
        S_coord[:, m_half-mplot_half:m_half+mplot_half+1], 
        b1_eff[:, m_half-mplot_half:m_half+mplot_half+1], cmap='jet', shading='gouraud')
    elif mode=='angle': c = axs.pcolormesh(
        m_coord[:, m_half-mplot_half:m_half+mplot_half+1], 
        S_coord[:, m_half-mplot_half:m_half+mplot_half+1], 
        b1_angle[:, m_half-mplot_half:m_half+mplot_half+1], cmap=cm, shading='gouraud', vmin=-np.pi, vmax=np.pi)
    fig.colorbar(c, ax=axs)

    marker_sep = 5 # Sometimes you want a sparse marker distribution along the line
    if Q is not None:
        axs.plot((-Q*n_har)[::marker_sep], S[::marker_sep], 'gx', linewidth=2)
    axs.set_xlabel('$m$', fontsize=20)
    axs.set_ylabel('$\psi_{pol}^{1/2}$', fontsize=20)
    axs.set_xlim([-mplot_half, mplot_half])
    axs.set_ylim([0,1.0])

    if mode=='abs': axs.set_title("$|\\tilde{b}^1_{m,n="+str(n_har)+"}|$ Spectrum", fontsize=20)
    elif mode=='angle': axs.set_title('$angle(\\tilde{b}^1_{m,n='+str(n_har)+'})$ Spectrum', fontsize=20)
    return fig, axs
