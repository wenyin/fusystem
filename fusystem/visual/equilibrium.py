import numpy as _np
from numpy import ndarray as _ndarray
import matplotlib.pyplot as _plt
from fusystem.field import Field2D as _Field2D
from pathlib import Path as _Path

def B_pol_section(R:_ndarray=None,Z:_ndarray=None, BR:_ndarray=None,BZ:_ndarray=None, fig=None,ax=None, cbar_mode:str='linear', field_avg:float=None, field_max:float=None, nocbar:bool=False, cax=None):
    from mpl_toolkits.axes_grid1.inset_locator import inset_axes
    from matplotlib import colors 

    if fig is None: fig, ax = _plt.subplots(1, 1, figsize=(5.0, 5.0*4/3)) 
    if nocbar: assert( field_max is not None and field_avg is not None)
    Rg, Zg = _np.meshgrid(R, Z)
    B_pol = _np.sqrt(BR**2 + BZ**2)
    if field_avg is None: field_avg = _np.average(B_pol) 
    if field_max is None: 
        field_max = _np.max(B_pol)
        cbar_up_extend = False
    else:
        cbar_up_extend = True
    strm_kwarg = {'density': 1.3}
    if cbar_mode=='linear':
        linewidth = 2.0 * B_pol.T / field_max
        strm_kwarg.update( {'linewidth':linewidth, 'color':B_pol.T, 'cmap':'gnuplot', 
                        'norm': colors.Normalize(vmin=0.0,vmax=field_max)})
    elif cbar_mode=='percent':
        p = 0.8
        B_pol_norm = (B_pol.T / field_max)**p # A big power greater than 1.0 will expand the [1.0-epsilon, 1.0], while a smaller value than 1.0 will shrink the high value part.
        linewidth = 0.4 * B_pol_norm / (field_avg/field_max)**p 
        strm_kwarg.update( {'linewidth':linewidth, 
                        'color':B_pol.T/field_max, 'cmap':'YlOrRd', 
                        'norm': colors.LogNorm(vmin=1e-4,vmax=1.)} )
    else: 
        raise ValueError("Wrong arg, linewidth_mode should be one of 'factor', 'percent'.")

    strm = ax.streamplot(Rg,Zg,BR.T,BZ.T, **strm_kwarg)
    if not nocbar:
        if cax is not None:
            axins = cax
            axins.cla()
        else:
            axins = inset_axes(ax,
                        width="5%",  # width = 5% of parent_bbox width
                        height="35%",  # height : 50%
                        loc='lower right',
                        # bbox_to_anchor=(1.05, 0., 1, 1),
                        # bbox_transform=ax.transAxes,
                        borderpad=0.5,
                        )
        # cbar.ax.set_title('$B_{pol} (T)$', fontsize=10)
        if cbar_mode=='linear':
            if cbar_up_extend: cbar = fig.colorbar(strm.lines, cax=axins, extend='max')
            else: cbar = fig.colorbar(strm.lines, cax=axins)
            cbar.set_label('$B_{pol} (T)$', y=1.10, labelpad=-5, rotation=0)
        elif cbar_mode=='percent':
            cbar = fig.colorbar(strm.lines, cax=axins, extend='min')
            cbar.set_label('$B_{pol} (\%) $', y=0.005, labelpad=-3.5, rotation=0)
        cbar.ax.tick_params(labelsize=8.0)
    # fig.colorbar(strm.lines, ax=ax, shrink=0.8) # The most primitive style colorbar

    ax.set_aspect('equal')
    ax.set_xlabel('$R$(m)', fontsize=8.5)
    ax.set_ylabel('$Z$(m)', fontsize=8.5)
    if cbar_mode=='linear':
        ax.set_title(" Poloidal Field Components Streamline\nat Poiloidal Cross-Section", fontsize=10)
    elif cbar_mode=='percent':
        ax.set_title(" Poloidal Field Components Streamline\nat Poiloidal Cross-Section\n(Color indicating the $B_{pol} / B_{pol~max}$)", fontsize=10)
    
    if not nocbar: return fig, ax, axins
    elif nocbar: return fig, ax

def S_section(R:_ndarray,Z:_ndarray, psi_norm:_ndarray, fig=None,ax=None):
    import matplotlib.pyplot as _plt

    if fig is None: fig, ax = _plt.subplots(1, 1, figsize=(5.0, 5.0*4/3))
    Rg, Zg = _np.meshgrid(R, Z)
    psi_norm_contour = ax.contour(Rg, Zg, psi_norm, levels=_np.arange(7)/5.0)
    ax.set_title("$\Psi_{norm}$ Distribution \n at Poloidal Cross-section", fontsize=10)
    ax.clabel(psi_norm_contour, inline=1, fontsize=10)
    ax.set_aspect('equal')
    ax.set_xlabel('$R$(m)', fontsize=8.5)
    ax.set_ylabel('$Z$(m)', fontsize=8.5)
    return fig, ax

def flt_pol_section(fig,ax, flt_lines):
    # TODO: now it only supports the fixde number of field line tracing. Make the markers generic.
    for i, color_marker in enumerate(['r*', 'g+', 'b^', 'g^', 'r+']):
        ax.plot(flt_lines[i][:,0],flt_lines[i][:,1], color_marker)

def S_levels(fig,ax, psi_norm_isosurface_rzphi:_ndarray):
    ax.plot(psi_norm_isosurface_rzphi[:,0,0], psi_norm_isosurface_rzphi[:,0,1])

def divertor_edge(fig,ax, div_edge:_ndarray):
    ax.plot(div_edge[:,0], div_edge[:,1])

# [Possible to make labels appear when hovering over a point in matplotlib? ImportanceOfBeingErnest's answer](https://stackoverflow.com/questions/7908636/possible-to-make-labels-appear-when-hovering-over-a-point-in-matplotlib) 
def S_isoline(fig, ax, r_mesh:_ndarray,z_mesh:_ndarray, TET:_ndarray, pad:int=2, S:_ndarray=None, s_chosen:float=None):
    if s_chosen is not None:
        ind = _np.argmin(_np.abs(S-s_chosen))
        assert(pad==2)
        pad = len(S) - ind

    sep_bound_line, = ax.plot(r_mesh[-pad,:],z_mesh[-pad,:], '--', linewidth=0.5) 
    annot = ax.annotate(
        "", xy=(0,0), xytext=(-20,20), textcoords="offset points",
        bbox=dict(boxstyle="round", fc="w"),
        arrowprops=dict(arrowstyle="->"))
    annot.set_visible(False)

    def update_annot(ind):
        x,y = sep_bound_line.get_data()
        annot.xy = (x[ind["ind"][0]], y[ind["ind"][0]])
        text = "$\\theta^*=$" + " ".join(["{0:.1f}°".format(TET[n]/(2*_np.pi)*360.0) for n in ind["ind"]]) 
        if S is not None: text += f"\n$s=${S[-pad]:.3f}"
        annot.set_text(text)
        annot.get_bbox_patch().set_alpha(0.4)

    def hover(event):
        vis = annot.get_visible()
        if event.inaxes == ax:
            cont, ind = sep_bound_line.contains(event)
            if cont:
                update_annot(ind)
                annot.set_visible(True)
                fig.canvas.draw_idle()
            else:
                if vis:
                    annot.set_visible(False)
                    fig.canvas.draw_idle()
    fig.canvas.mpl_connect("motion_notify_event", hover)

# Drwa a mesh whose lines indicate the \theta^* invariant lines.
def TET_star_isolines(fig, ax, r_mesh:_ndarray, z_mesh:_ndarray, line_per_num=10):
    ntheta = r_mesh.shape[1]
    for i in range(ntheta):
        if i % line_per_num==0: ax.plot(r_mesh[:-1,i],z_mesh[:-1,i], 'r', linewidth=0.3) 

def coil_proj_RZ(fig, ax, machine_name:str, coil_sel_dict:dict):
    from fusystem import file, coord
    ylim = ax.get_ylim()
    coil_r_xyz, coil_info = file.CoilRead(machine_name, coil_sel_dict.keys())
    for coilsys in coil_sel_dict.keys():
        for coil in coil_sel_dict[coilsys]:
            coil_r_rzphi_one = coord.coord_system_change(coord_from=coil_info[coilsys][coil]['cor'], coord_to='RZPhi', r=coil_r_xyz[coilsys][coil], merge_return=True)
            ax.plot(coil_r_rzphi_one[...,0], coil_r_rzphi_one[...,1], '--', linewidth=0.5, label=coil)
    ax.legend()
    ax.set_ylim(ylim)

