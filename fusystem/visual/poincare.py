
def draw(fig, ax, poincare_RZ):
    for i in range(len(poincare_RZ)):
        ax.scatter(poincare_RZ[i][:,0], poincare_RZ[i][:,1], s=0.05, c='grey')