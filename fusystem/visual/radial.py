import numpy as _np
from numpy import ndarray as _ndarray
import matplotlib.pyplot as _plt

def island_chain_calc(tilde_b1_mn:_ndarray, Q:_ndarray, S:_ndarray, Phi:float=0.0, s_range=None, fig=None,ax=None, max_mn=[10,4]):
    from ergospy import rational 
    all_q_mn_list = rational.all_q_mn_list(max_mn[0],max_mn[1], q_min=_np.min(Q), q_max=_np.max(Q))
    from scipy.interpolate import splev, splrep, PPoly
    q_s_spl = splrep(S, Q)
    q_s = PPoly.from_spline(q_s_spl)
    # dq_s =q_s.derivative()

    TET_arr = _np.linspace(-_np.pi, _np.pi, 10000, endpoint=True)
    
    s_HW_list = []
    for q_mn_list in all_q_mn_list:
        m,n = min([mn[0] for mn in q_mn_list]), min([mn[1] for mn in q_mn_list]) # m, n always positive
        q = float(m)/float(n)
        assert( q==q_mn_list[0][0]/q_mn_list[0][1] ) # q should be always the same with all other elements in the list
        Chi_arr = TET_arr - q * Phi
        s_arr = q_s.solve(q, extrapolate=False)
        
        for s in s_arr:
            if s_range is not None:
                if s<s_range[0] or s>s_range[1]: continue
            s_ind = _np.argmin(_np.abs(S - s))
            dq = splev(s, q_s_spl, der=1) # dq_s(s)
            if dq > 0: # TODO: what about negative dq/ds?
                bcos_sum_arr = _np.zeros_like(Chi_arr)
                for mn2 in q_mn_list:
                    k = int(mn2[0]/m) # how many orders is the harmonic?
                    bcos_sum_arr += 2*_np.abs(tilde_b1_mn[s_ind,k*m,-k*n]) /(k*m) \
                        * _np.cos(k*m*Chi_arr + _np.angle(tilde_b1_mn[s_ind,k*m,-k*n]) + _np.pi/2)
                island_halfwidth = _np.sqrt(2* q**2/dq * _np.ptp(bcos_sum_arr))
                s_HW_list.append([s, m,n, island_halfwidth].copy())

                if ax is not None:
                    for int_const in _np.linspace(
                        -_np.min(bcos_sum_arr)-_np.ptp(bcos_sum_arr)*2/3, 
                        -_np.min(bcos_sum_arr)+_np.ptp(bcos_sum_arr)*2/3, 5): # Just draw 5 lines per island chain
                        with _np.errstate(invalid='ignore'):
                            s_bar_arr = _np.sqrt(2* q**2/dq * (bcos_sum_arr + int_const))
            #             print(_np.split(s_bar_arr, [~_np.isnan(s_bar_arr)[0]]))
            #             Chi_arrremoved_ = Chi_arr[_np.logical_not(_np.isnan(s_bar_arr))]
            #             s_bar_removed_ = s_bar_arr[_np.logical_not(_np.isnan(s_bar_arr))]
                        line_TET_ = _np.concatenate((TET_arr, _np.flipud(TET_arr)))
                        line_s_bar_=_np.concatenate((s_bar_arr,_np.flipud(-s_bar_arr)))
                        ax.plot(line_TET_, s + line_s_bar_, '-', alpha=0.4, linewidth=0.5) 
    s_HW_list.sort(key=lambda e:e[0])
    return s_HW_list

def island_chain_plot(tilde_b1_mn:_ndarray, Q:_ndarray, S:_ndarray, Phi:float=0.0, polar=False, s_range:_ndarray=None, max_mn=[10,4]):
    figsize = (10.0, 10.0) if polar else (10.0, 5.5)
    fig, ax = _plt.subplots(1, 1, figsize=figsize, subplot_kw=dict(polar=polar))

    island_chain_calc(tilde_b1_mn, Q=Q, S=S, Phi=Phi, s_range=s_range, fig=fig,ax=ax, max_mn=max_mn)

    if polar and s_range is not None:
        ax.set_rorigin(s_range[0]-0.12)
        ax.set_rmin(s_range[0]-0.02); ax.set_rmax(s_range[1]+0.02)
    return fig, ax

def q_s_plot(Q:_ndarray, S:_ndarray, s_range=None,fig=None,ax=None, polar=True):
    from scipy.interpolate import splev, splrep
    if ax is None:
        if polar:
            fig, ax = _plt.subplots(1, 1, figsize=(6.0, 6.0/2*4/3), subplot_kw=dict(polar=True))
        else:
            fig, ax = _plt.subplots(1, 1, figsize=(6.0, 6.0/2*4/3))
    else:
        if ax.name == 'polar' and polar: 
            pass
        elif not polar: 
            pass
        else:
            raise ValueError("Polar plot is chosen while the given ax is not for polar plot.")

    q_s_spl = splrep(S, Q)
    s_arr = _np.linspace(0.00, 1.0, 10000)
    q_ = splev(s_arr, q_s_spl)

    if polar:
        ax.set_thetamin(45); ax.set_thetamax(90)
        if s_range is not None:
            ax.set_rorigin(s_range[0]-0.15)
            ax.set_rmin(s_range[0]); ax.set_rmax(s_range[1])
            ax.set_rgrids(_np.linspace(s_range[0], s_range[1], 6, endpoint=True)) 
            ax.set_thetagrids(_np.rad2deg(_np.linspace(_np.pi/4, _np.pi/2, 5, endpoint=True))) 
    
    if s_range is None: s_range = [_np.min(s_arr), _np.max(s_arr)]
    s_chosen_arr = _np.logical_and(s_range[0]<=s_arr, s_arr<=s_range[1])
    
    if polar:
        ax.plot(
            _np.arctan(q_[s_chosen_arr]), 
            s_arr[s_chosen_arr], color='#ee8d18', lw=0.5)
        # ax.set_theta_zero_location('E', offset=-45)
    else:
        ax.plot(
            s_arr[s_chosen_arr], 
            q_[s_chosen_arr], color='#ee8d18', lw=0.5)
    return fig, ax

def island_width_line_add(fig, ax, s:float, m:int, n:int, island_halfwidth:float):
    q = float(m)/float(n)
    if ax.name=='polar':
        island_line, = ax.plot(
            [_np.arctan(q)]*3, 
            [s-island_halfwidth, s, s+island_halfwidth], label=f'{m}/{n}')
    else:
        island_line, = ax.plot(
            [s-island_halfwidth, s, s+island_halfwidth], 
            [q]*3, label=f'{m}/{n}')

    annot = ax.annotate(
        "", xy=(0,0), xytext=(-20,20), textcoords="offset points",
        bbox=dict(boxstyle="round", fc="w"),
        arrowprops=dict(arrowstyle="->"))
    annot.set_visible(False)

    def update_annot(ind):
        x,y = island_line.get_data()
        annot.xy = (x[ind["ind"][0]], y[ind["ind"][0]])
        text = "$q=$"+f"{m}/{n}" 
        annot.set_text(text)
        annot.get_bbox_patch().set_alpha(0.4)

    def hover(event):
        vis = annot.get_visible()
        if event.inaxes == ax:
            cont, ind = island_line.contains(event)
            if cont:
                update_annot(ind)
                annot.set_visible(True)
                fig.canvas.draw_idle()
            else:
                if vis:
                    annot.set_visible(False)
                    fig.canvas.draw_idle()
    fig.canvas.mpl_connect("motion_notify_event", hover)