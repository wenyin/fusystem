# File: read_coils_table.py
# Author: Wenyin Wei wenyin.wei.ww@gmail.com Tsinghua Univ. & EAST
# Usage: Read the .xlsx file which stores the geometry info.
# Output: The dict() object X, Y, Z like the form 
#      {'low_n_coils_UP1':_np.array([...]), 
#      'low_n_coils_UP2':_np.array([...]), ..., 
#      'high_m_coils_2':_np.array([...])}.
#      coil_info: dict('high_m_coils_2': dict{'sheet':'2', 'current':500, 'machine':'EAST' }...)

from pathlib import Path as _Path
import numpy as _np
# This is a trick to keep a global variable, from timmwagener, https://stackoverflow.com/questions/1977362/how-to-create-module-wide-variables-in-python
import sys as _sys
this = _sys.modules[__name__]
this._path_data = None

## Path 
def SetDataPath(data_path:_Path):
    this._path_data = data_path
    return this._path_data

def PathMachine(machine:str):
    """Function to acquire the path to the queried machine.
    Args:
        machine (str): For which machine one wants to query the path? 
    Returns:
        pathlib.Path: path to the specified machine
    """
    return this._path_data / machine

## Coil
def CoilsysNames(machine:str):
    """List all the coil system names for chosen machine.
    Args:
        machine (str): For which machine you want to list the coil system names?
    Returns:
        coilsys_names (list of str): 
    """
    path_coils = PathMachine(machine) / 'coils'
    paths_coilsys = [p for p in path_coils.iterdir() if p.is_dir()]
    coilsys_names = [str(p.name) for p in paths_coilsys]
    print(coilsys_names)
    return coilsys_names

_coil_param_must = ['turns', 'unit', 'cor']
def CoilRead(machine_name, coilsys_names=None, only_info:bool=False, ret_xcor:str=None, silent=True):
    """Coil reading function

    Args:
        machine_name (str): for which machine the user wants to read the coil.
        coilsys_names ([type], optional): for which coil systems the user wants to read. Defaults to None and read coils belong to all of coil systems.
        only_info (bool, optional): Reading coil structure array is time-consuming, users could turn this on to lessen the reading time needed. Defaults to False.
        ret_xcor (str, optional): auto transform the coil structure arrays into a specified x coordinate system ('XYZ', 'RZPhi' or etc.). Defaults to None and return structure arrays in the original coordinate system.
        silent (bool, optional): whether or not to print coil infomation on the fly. Defaults to True and nothing would be outputed.

    Raises:
        RuntimeError: [description]
        RuntimeWarning: [description]
        ValueError: [description]
        RuntimeError: [description]

    Returns:
        [type]: [description]
    """    
    # import the modules in the function scope in case of polluting namespace. 
    import openpyxl
    import numpy as np
    import fusystem.coord as coord
    
    if isinstance(coilsys_names, str):
        raise RuntimeError("Python looping on a string would loop on its each character, to avoid such a case, use a list [] to pack your coil system name.") 
        
    path_machine = PathMachine(machine_name) 

    r_coils, coil_info = dict(), dict()
    if coilsys_names is None: 
        coilsys_names = CoilsysNames(machine_name)
    for coilsys in coilsys_names:
        # 打开 excel 文件,获取工作簿对象
        try:
            assert(isinstance(coilsys, str))
            if len(coilsys)==1: print("Make sure your coilsys is a string rather than a character looped on a string.")
            wb = openpyxl.load_workbook( path_machine / 'coils' / coilsys / 'field_computation' / (coilsys+'.xlsx'))
        except:
            raise RuntimeWarning(f"{coilsys} coil system does not have coil excel table file.")
        r_coils[coilsys], coil_info[coilsys] = dict(), dict()
        # 从表单中获取单元格的内容
        for coil in wb.sheetnames:
            ws = wb[coil]  # 当前选定的表单 Currently working sheet
            X = np.asarray([cell.value for cell in ws['B']], dtype=np.float32) if not only_info else np.zeros((1))
            Y = np.asarray([cell.value for cell in ws['C']], dtype=np.float32) if not only_info else np.zeros((1))
            Z = np.asarray([cell.value for cell in ws['D']], dtype=np.float32) if not only_info else np.zeros((1))
            r_coils[coilsys][coil] = np.column_stack((X, Y, Z))
            
            single_coil_param = dict()
            
            # Read the parameter column
            param_row = 1
            while True:
                Gcell = 'G'+str(param_row) # The cell of G column, param_row row
                Hcell = 'H'+str(param_row) # The cell of G column, param_row row
                single_coil_param[ws[Gcell].value] = ws[Hcell].value
                param_row += 1
                if not ws[Gcell].value: break
                if param_row > 100: raise ValueError(f"The excel has too many parameters, check if the excel file {coilsys} is similar to other files.")
            single_coil_param['sheet'] = coil
            
            # Check if there is necessary parameter illegal
            lack_param = []
            for param in _coil_param_must:
                try: single_coil_param[param]
                except NameError: lack_param.append(param)
            if len(lack_param)!=0:
                raise RuntimeError('Please supplement the necessary parameters in columns G & H'+str(lack_param))
                    
            # Let the current satisfy the kA*turn = 1
            single_coil_param['current_1kAt'] = 1000.0/float(single_coil_param['turns'])
            if 'current_limit' in single_coil_param:
                single_coil_param['current_limit'] = 1000.0*float(single_coil_param['current_limit'])

            
            if not only_info:
                # Adjust to different coord system and unit scale 
                if single_coil_param['cor']=='XYZ' and single_coil_param['unit']=='mm/mm/mm':
                    r_coils[coilsys][coil] /= 1000.0
                if ret_xcor is not None:
                    r_coils[coilsys][coil] = coord.coord_system_change(
                        single_coil_param['cor'], ret_xcor, r_coils[coilsys][coil]) 
                    
                # Mirror the coil if required
                if 'mirror' in single_coil_param:
                    r_coils[coilsys][coil+'_mirror_'+single_coil_param['mirror']] = coord.coord_mirror(
                        single_coil_param['cor'], r_coils[coilsys][coil], single_coil_param['mirror'])
            
            # Append the coil parameters to the coil_info dictionary
            coil_info[coilsys][coil] = single_coil_param
            if 'mirror' in single_coil_param: 
                single_mirror_param = single_coil_param.copy()
                coil_info[coilsys][coil+'_mirror_'+single_coil_param['mirror']] = single_mirror_param

            # Report the situation if required
            if not silent:
                for key, value in single_coil_param.items(): 
                    print(f"{key} : {value}")
                if 'mirror' in single_coil_param: print(f"There is a mirror about plane {single_coil_param['mirror']}")
                print("")
    return r_coils, coil_info

### Reference
# For openpyxl operations on excel table files, refer [python 读取 Excel](https://www.cnblogs.com/crazymagic/articles/9752287.html)
# [Python odule openpyxl Tutorial](https://openpyxl.readthedocs.io/en/stable/tutorial.html)

def CoilWrite(machine_name, coilsys, coil_data:list, coil_info:dict):
    """Write coil structure and info in a standard, reusable and clean way. 

    Args:
        machine_name (str): for which machine this coil belongs to.
        coilsys (str): for which coilsys this coil belongs to.
        coil_data (list): coil structure array
        coil_info (dict): coil information dictionary

    Raises:
        RuntimeError: [description]
    """    
    from openpyxl import Workbook
    assert(isinstance(machine_name, str) and isinstance(coilsys, str))
    assert(len(coil_info)==len(coil_data))

    # Check if the coil_info format is correct
    lack_param = []
    for coil in coil_info.keys():
        for param in _coil_param_must:
            try:
                coil_info[coil][param]
            except NameError:
                lack_param.append(coil+'_'+param)
    if len(lack_param) > 0:
        raise RuntimeError("Please supplement the necessary parameters in the corresponding coil "+ " ".join(lack_param))
    coil_names = list(coil_info.keys())

    # Path initialization
    path_coilxlsx_folder = PathMachine(machine_name) / coilsys / 'field_computation' 
    path_coilxlsx_folder.mkdir(parents=True, exist_ok=True)

    wb = Workbook()
    for i in range(len(coil_names)):
        if i==0:
            ws = wb.active
            ws.title = coil_names[0]
        else:
            ws = wb.create_sheet(title=coil_names[i])
        for r in range(coil_data[i].shape[0]):
            for c in range(coil_data[i].shape[1]):
                ws.cell(row = r+1, column = c+2).value = coil_data[i][r, c]
        for ind, key in enumerate( coil_info[coil_names[i]].keys() ):
            ws.cell(row = ind+1, column = 7).value = key
            ws.cell(row = ind+1, column = 8).value = coil_info[coil_names[i]][key]
    wb.save(filename = path_coilxlsx_folder / (coilsys+'.xlsx'))

## Collaborative Coils
def CollabTableBuild(machine_name='EAST', overwrite=False):
    """Build a primitive default collaboration .xlsx table. 

    Args:
        machine_name (str, optional): [description]. Defaults to 'EAST'.
        overwrite (bool, optional): [description]. Defaults to False.

    Raises:
        RuntimeError: [description]
        RuntimeWarning: [description]
        RuntimeError: [description]
        RuntimeError: [description]
        RuntimeError: [description]
    """    
    from openpyxl import Workbook
    path_machine = PathMachine(machine_name)
    file_collab_xlsx = path_machine / 'collab.xlsx'
    path_mac_other = [path_machine / 'equilibria', path_machine / 'divertor']
    if not path_machine.exists():
        raise RuntimeError("The machine name i_nput is illegal, check whether the machine folder "+machine_name+" exists.") 
    if file_collab_xlsx.exists() and not overwrite:
        raise RuntimeWarning("The machine already has an `collab.xlsx` file indicating their collaborative relationship. You need to let CollabTableBuild(..., overwrite=False) to overwrite the file. Be careful when you want to overwrite it, as there are many parameters need to be setup manually.") 
    [p.mkdir(exist_ok=True) for p in path_mac_other]

    _, coil_info = CoilRead('EAST', only_info=True)
    # Prepare Workbook
    wb = Workbook()
    manual_coilsys = ["Collab"]
    for i, coilsys in enumerate(coil_info.keys()):
        if i==0:
            ws = wb.active
            ws.title = coilsys
        else:
            ws = wb.create_sheet(title=coilsys)

        for coil_ind, key in enumerate( coil_info[coilsys].keys() ):
            ws.cell(row = coil_ind+2, column = 1).value = key
        
        work_mode_s = []
        work_mode_col = 2 
        if coilsys=="low_n_coils":
            up_and_down_coil_num = int(len(coil_info[coilsys])/2)
            low_n_space_rad = 2 * _np.pi / up_and_down_coil_num
            machine_phi0_rad = -33.7276642 * (2 * _np.pi / 360)

            for n in range(2, 4):
                for mode in ["dPhi="+str(i*10) for i in range(36)]: # + ["odd", "even"]
                    work_mode = f"n={n}_{mode}"
                    work_mode_s.append(work_mode)
                    ws.cell(row = 1, column = work_mode_col).value = work_mode

                    # Setup the UP and DOWN phi
                    phi_up_rad = 0.5
                    if mode=="even":
                        phi_down_rad = phi_up_rad
                    elif mode=="odd":
                        phi_down_rad = phi_up_rad-_np.pi 
                    elif mode[:4]=="dPhi":
                        phi_down_rad = phi_up_rad-float(mode.split("=")[1])/360*(2*_np.pi)
                    else:
                        raise RuntimeError("Not yet prepared low_n_coils work mode.")

                    for coil_ind, key in enumerate( coil_info[coilsys].keys() ):
                        if key.find("UP")>=0:
                            coil_number = int(key[2:])
                            phi_coil_rad = low_n_space_rad * (coil_number-1)
                            ws.cell(row = coil_ind+2, column = work_mode_col).value = _np.cos(n* phi_coil_rad - phi_up_rad)
                        elif key.find("DOWN")>=0:
                            coil_number = int(key[4:])
                            phi_coil_rad = low_n_space_rad * (coil_number-1)
                            ws.cell(row = coil_ind+2, column = work_mode_col).value = _np.cos(n* phi_coil_rad - phi_down_rad)
                        else:
                            raise RuntimeError("The coil in low_n_coils is named neither UPn nor DOWNn. Please check your naming convention.")
                    work_mode_col += 1
                
        elif coilsys=="high_m_coils":
            for work_mode in ["primary", "secondary"]:
                work_mode_s.append(work_mode)
                ws.cell(row = 1, column = work_mode_col).value = work_mode
                for coil_ind, key in enumerate( coil_info[coilsys].keys() ):
                    if key.find("mirror")>=0:
                        # coil_number = int(key[2:])
                        # phi_coil_rad = low_n_space_rad * (coil_number-1)
                        ws.cell(row = coil_ind+2, column = work_mode_col).value =-1.0 if work_mode=="secondary" else 1.0
                    elif key.find("UP")>=0:
                        # coil_number = int(key[2:])
                        # phi_coil_rad = low_n_space_rad * (coil_number-1)
                        ws.cell(row = coil_ind+2, column = work_mode_col).value = 1.0
                    else:
                        raise RuntimeError("The coil in high_m_coils is not named correctly. Please check your naming convention.")
                work_mode_col += 1
        else:
            manual_coilsys.append(coilsys)

    ws = wb.create_sheet(title='Collab')
    ws.cell(row = 1, column = 1).value = "Coil_System"
    ws.cell(row = 1, column = 2).value = "Coil"
    wb.save(filename = file_collab_xlsx)
    print("The corresponding excel table file has been saved at:", path_machine / 'collab.xlsx')
    print("Please supplement in the following coil system sheet how their coils coorporate:\n", manual_coilsys)
    print("Note that the `Collab` sheet would to collaborate various coil system to achieve a better performance.")

## Field
def FieldWrite(path_to_save, r, param:dict, xcor:str, fcor:str, overwrite=False):
    """Write the 3D field into disk in a standard, elegant and clean way.  

    Args:
        path_to_save (pathlib.Path): where would the user like to store the field data files
        r ([type]): [description]
        param (dict): the field parameters in a phthonic dictionary
        xcor (str): the coordinate system to store the field location. 
        fcor (str): the coordinate system to store the field value components.
        overwrite (bool, optional): whether or not to overwrite the existing field data. Defaults to False and notify users to be careful when overwriting.

    Raises:
        RuntimeWarning: [description]
    """    
    path_to_save.mkdir(parents=True, exist_ok=True)
    file_param = path_to_save / f'B_{xcor}_{fcor}_param.npz'
    if file_param.exists(): file_param.unlink()

    file_to_save = path_to_save / f'B_{xcor}_{fcor}.npy'
    if not overwrite and file_to_save.exists():
        raise RuntimeWarning("The path to save field data has been occupied. Make sure you want to overwrite it and set the arg `exist_ok` to be True.")
    
    _np.save(file_to_save, r)
    _np.savez(file_param, **param)
        
# Attention, remove_n_eq_0 should be used when the field coordinate are in the cylindrical system. 
def FieldRead(path_to_read, xcor, fcor, rot_phi_rad:float=None, remove_n_eq_0:bool=False):
    """Field data reading function

    Args:
        path_to_read (pathlib.Path): from where to read the field data
        xcor (str): the coordinate system to store the field location. 
        fcor (str): the coordinate system to store the field value components.
        rot_phi_rad (float, optional): how many radians do the user want to rotate the field. Defaults to None.
        remove_n_eq_0 (bool, optional): whether or not to strip the phi-direction average value (n=0 components in spectrum). Defaults to False.

    Returns:
        [type]: [description]
    """    
    path_npy = path_to_read / f'B_{xcor}_{fcor}.npy'
    field_np = _np.load(path_npy)

    if rot_phi_rad:
        assert (xcor=='RZPhi' or xcor=='STETPhi')
        shift_grid_num = int(rot_phi_rad / (2*_np.pi) * nphi)
        field_np = _np.roll(field_np, shift_grid_num, axis=2)

    # If n=0 spectrum is removed, that is equivalent to considering the fluctuation around the phi-average value.
    if remove_n_eq_0:
        assert (xcor=='RZPhi' or xcor=='STETPhi')
        assert (field_np.shape[-1]==3)
        for i in range(3):
            field_comp_av = _np.average(field_np[..., i], axis=-1)
            field_np[..., i] = field_np[..., i] - field_comp_av[:,:,None]
    
    return field_np

def FieldMeshRead(path_field_data, xcor, fcor): 
    """The function to only read the field mesh.

    Args:
        path_field_data (pathlib.Path): where to read the field mesh
        xcor (str): the coordinate system to store the field location. 
        fcor (str): the coordinate system to store the field value components.

    Returns:
        dict : [description]
    """    
    param = dict(_np.load(path_field_data / f'B_{xcor}_{fcor}_param.npz'))
    if xcor=='RZPhi':
        nR,nZ,nPhi = param['meshsize']
        param['nR'] = nR
        param['nZ'] = nZ
        param['nPhi']= nPhi
        Rmin,Rmax, Zmin,Zmax = param['RZlimit']
        param['R'] = _np.linspace(Rmin, Rmax, nR) # [Rmin, Rmin+dR, ..., Rmax] totally nR elements.
        param['Z'] = _np.linspace(Zmin, Zmax, nZ) # [Zmin, Zmin+dZ, ..., Zmax] totally nZ elements.
        param['Phi'] = _np.arange(0, 2*_np.pi, 2*_np.pi/nPhi) #[0, dPhi, ..., 2π-dPhi] totally _nphi elements.
    elif xcor=='STETPhi':
        ns, ntheta = param['r_mesh'].shape
        nPhi = param['meshsize'][2]
        param['nPhi'], param['Phi'] = nPhi, _np.arange(0, 2*_np.pi, 2*_np.pi/nPhi)
        param['ns'] = ns
        param['ntheta']=ntheta
    return param

def FieldParamRead(path_to_read, xcor:str, fcor:str):
    field_param = path_to_read / f'B_{xcor}_{fcor}_param.npz'
    if not field_param.exists():
        raise ValueError("Check whether you have calculated the field data and have its param _npz file.")
    field_param = dict(_np.load(field_param))
    return field_param

def FieldDataIfRecyclable(path_to_save, param:dict) -> bool:
    """Judge whether the field data exists and recyclable to satisfy requirements from arg param.

    Args:
        path_to_save (pathlib.Path): the path where the field data should be stored 
        param (dict): parameters required for the field data. If the existing data does not satisfy the requirement from arg param, this function would return False. 

    Returns:
        bool: whether or not the field data exists and is recyclable. 
    """    
    xcor, fcor = param['xcor'], param['fcor']
    try: written_param = FieldParamRead(path_to_save, xcor,fcor)
    except: return False
    if xcor=='RZPhi':
        if not _np.equal(written_param['meshsize'], param['meshsize'], dtype=_np.int).all(): return False
        if not _np.equal(written_param['RZlimit'],  param['RZlimit'], dtype=_np.float32).all(): return False
    elif xcor=='STETPhi':
        if not _np.equal(written_param['meshsize'], param['meshsize'],dtype=_np.int).all(): return False
        if not written_param['equilib'] == param['equilib']: return False
    return True

## Divertor
def DivertorRead(machine_name:str, divertor_name:str):
    """Divertor data files reading function

    Args:
        machine_name (str): for which machine the user wants to query the divertor structure. e.g. 'EAST'.
        divertor_name (str): for which divertor of the machine the user wants to load. e.g. 'EAST_divertor.dat'.

    Raises:
        RuntimeError: [description]

    Returns:
        [type]: [description]
    """    
    import numpy
    path_diver = PathMachine(machine_name) / 'divertor' / divertor_name
    diver = numpy.loadtxt(path_diver, skiprows=1)
    # Make sure the divertor is closed.
    assert( (diver[0, 0:3:2]==diver[-1, 1:4:2]).all() )
    # Make sure the each line segment is correct with coherent points.
    for i in range(diver.shape[0]):
        if i!=0:
            if not (diver[i, 0:3:2]==diver[i-1, 1:4:2]).all():
                # print("The divertor data is not connected simply by a line, the code cannot handle it for the time being.")
                raise RuntimeError("The divertor data is not connected simply by a line, the code cannot handle it for the time being. The bifurcation row is located at "+str(i) )
    return numpy.concatenate( (diver[:, 0:3:2], [diver[-1, 1:4:2]]) )


#######################################################
# EFIT Part
#######################################################
def read_geqdsk(gfile:str) -> dict:
    """Read the gfile describing the plasma equilibrium state.

    Args:
        gfile (str): [description]

    Returns:
        dict: [description]

    Error:
        There are still some variables and the code does not know where they come from, e.g. nfcoil and nesum. But they are at the latter part of the code, so ask for help from Wenyin when you encounter unknown variable problem.
    """
    
    def read_numeric(f) -> str: 
        numstr = ''
        char_one = f.read(1)
        numstr += char_one
        while True:
            char_one = f.read(1)
            numstr += char_one
            if char_one == 'e' or char_one == 'E':
                numstr += f.read(3)
                return numstr.strip()
            elif char_one == ' ' and numstr[-2] in ['0', '1','2','3','4','5','6','7','8','9']:
                return numstr.strip()
            elif char_one == '\n':
                return numstr.strip()

    iecurr = 0
    with open(gfile, mode='r') as f:
        efit = {}
        if isinstance(gfile, _Path):
            efit['gfile'] = gfile.name
        elif isinstance(gfile, str):
            efit['gfile'] = gfile
        else:
            raise ValueError("Wrong gfile format")
        
        efit['cas'] = str(f.read(48))
        idum = str(f.read(4))
        efit['nw'] = nw = int(read_numeric(f))
        efit['nh'] = nh = int(read_numeric(f))        
        efit['nprof'] = nprof = nw

        # floats
        for key in ['xdim', 'zdim', 'rzero', 'rmin', 'zmid', 'rmaxis', 'zmaxis', 'ssimag', 'ssibry', 'bcentr', 'cpasma', 'ssimag', 'xdum', 'rmaxis']:
            efit[key] = float(read_numeric(f))
        xdum = float(read_numeric(f))
        efit['zmaxis'] = float(read_numeric(f))
        xdum = float(read_numeric(f))
        efit['ssibry'] = float(read_numeric(f))
        xdum = _np.array([float(read_numeric(f)), float(read_numeric(f))])

        for key in ['fpol', 'pres', 'ffprim', 'pprime']:
            efit[key] = _np.empty((nprof))
            for i in range(nprof):
                efit[key][i] = float(read_numeric(f))

        # Note EFIT store psi as a matrix [z index, r index] (Cartesian order), while in ERGOS it is matrix order. Remember to reverse the index order by psi.transpose() into [r index, z index].
        efit['psirz'] = _np.empty((nh, nw))
        for i in range(nw):
            for j in range(nh):
                efit['psirz'][i,j] = float(read_numeric(f))
        efit['qpsi'] = _np.empty((nprof))
        for i in range(nprof):
            efit['qpsi'][i]   = float(read_numeric(f))
                
        efit['nbbbs'] = int(read_numeric(f))
        efit['limitr'] = int(read_numeric(f))
        
        efit['rbbbs'] = _np.empty((efit['nbbbs']))
        efit['zbbbs'] = _np.empty((efit['nbbbs']))
        for i in range(efit['nbbbs']): 
            efit['rbbbs'][i] = float(read_numeric(f))
            efit['zbbbs'][i] = float(read_numeric(f))
        
        efit['xlim'] = _np.empty((efit['limitr']))
        efit['ylim'] = _np.empty((efit['limitr']))
        for i in range(efit['limitr']): 
            efit['xlim'][i] = float(read_numeric(f))
            efit['ylim'][i] = float(read_numeric(f))

        #--  read rotation information, if available
        efit['kvtor'] = int(read_numeric(f))
        efit['rvtor'] = float(read_numeric(f))
        efit['nmass'] = int(read_numeric(f))
        if efit['kvtor']:
            for key in ['pressw', 'pwprim']:
                efit[key] = _np.empty((nprof))
                for i in range(nprof):
                    efit[key][i] = float(read_numeric(f))
        
        # The following code has not yet been test, and i_t i_t still unknown where some variables come from, like nfcoil, neum.

        #--  read ion mass density profile if available
        if efit['nmass']:
            for key in ['dmion', 'rhovn']:
                efit[key] = _np.empty((nprof))
                for i in range(nprof):
                    efit[key][i] = float(read_numeric(f))
                    
            efit['keecur'] = int(read_numeric(f))
            if efit['keecur']:
                efit['epoten'] = _np.empty((nprof))
                for i in range(nprof):
                    efit['epoten'][i] = float(read_numeric(f))

        if iecurr:
            # int
            efit['mw'] = int(read_numeric(f))
            efit['mh'] = int(read_numeric(f))
            efit['ishot'] = int(read_numeric(f))
            efit['itime'] = int(read_numeric(f))
            # float
            efit['rmin'] = float(read_numeric(f))
            efit['rmax'] = float(read_numeric(f))
            efit['zmin'] = float(read_numeric(f))
            efit['zmax'] = float(read_numeric(f))
            efit['brsp'] = _np.empty((nfcoil))
            efit['ecurrt'] = _np.empty((nesum))
            efit['pcurrt'] = _np.empty((nh, nw))
            for i in range(nfcoil):
                efit['brsp'][i] = float(read_numeric(f))  
            for i in range(nesum):
                efit['ecurrt'][i] = float(read_numeric(f))  
            for i in range(nw): 
                for j in range(nh):
                    efit['pcurrt'][i, j] = float(read_numeric(f))  

    return efit

# def Read_g_File(machine:str, gfile:str):
#     from ..file import PathMachine
#     p_machine = PathMachine(machine)
#     read_geqdsk(p_machine / gfile)
#     return efit

def psi_norm_calc(efit:dict, nZ:int, nR:int):
    """[summary]

    ChangeLog:
        2008/09/22, Normalise EFIT psi to 1 on the separatrix and 0 on the magnetic axis and saves i_t as an input file for poincare_footprint.f.
    """

    psi_norm = _np.empty((nZ+1, nR+1))
    psi_norm[:,:] = (efit['psirz'][:,:]-efit['ssimag'])/(efit['ssibry']-efit['ssimag'])

    rgrid = _np.linspace(
        efit['rmin'], 
        efit['rmin'] + efit['xdim'], 
        num=efit['nw'])
    zgrid = _np.linspace(
        efit['zmid'] - 0.5*efit['zdim'], 
        efit['zmid'] + 0.5*efit['zdim'], 
        num=efit['nh'])
    

    psi_sep   = efit['ssibry']
    psi_maxis = efit['ssimag']
    # efit.psirz is apparently always increasing from the core to the edge,
    # whatever the plasma current direction, so we have to multiply by
    # -sign(efit.cpasma) to get the correct helicity
    psi_sep   *= -_np.sign(efit['cpasma'])
    psi_maxis *= -_np.sign(efit['cpasma'])

    return rgrid, zgrid, psi_norm, psi_sep, psi_maxis 

def _BR_calc(efit:dict, nZ:int, nR:int, Rgrid, zgrid):

    BR = _np.empty((nZ, nR))
    BR[1:,1:] = -(efit['psirz'][2:nZ+1,1:nR]-efit['psirz'][0:nZ-1,1:nR]) / (zgrid[2:nZ+1, None]-zgrid[0:nZ-1, None])
    BR[:,1:] = BR[:,1:]/Rgrid[None, 1:nR]
    BR[0,1:] = BR[1,1:]
    BR[1:,0] = BR[1:,1]
    BR[0, 0] = BR[0, 1] 

    # efit.psirz is apparently always increasing from the core to the edge,
    # whatever the plasma current direction, so we have to multiply by
    # -sign(efit.cpasma) to get the correct helicity
    BR *= -_np.sign(efit['cpasma'])
    return BR

def _BZ_calc(efit:dict, nZ:int, nR:int, Rgrid, zgrid):

    BZ = _np.empty((nZ, nR))
    BZ[1:nZ,1:nR] = (efit['psirz'][1:nZ,2:nR+1]-efit['psirz'][1:nZ,0:nR-1]) / (Rgrid[None,2:nR+1]-Rgrid[None,0:nR-1])
    BZ[:,1:] = BZ[:,1:] / Rgrid[None, 1:nR]
    BZ[0,1:] = BZ[1,1:]
    BZ[1:,0] = BZ[1:,1]
    BZ[0, 0] = BZ[0, 1]

    # efit.psirz is apparently always increasing from the core to the edge,
    # whatever the plasma current direction, so we have to multiply by
    # -sign(efit.cpasma) to get the correct helicity
    BZ = -_np.sign(efit['cpasma'] )* BZ
    return BZ

def _Bt_full_calc(efit:dict, nZ:int, nR:int, Rgrid, zgrid, psi_norm):
    """[summary]

    ChangeLog:
        E. Nardon 16/01/09, Calculates the full toroidal magnetic field including the plasma effect.
        The difficulty is that we have from the EFIT g-file the variable efit.fpol giving us the valu of R*Bt inside the plasma as a function of psi (R*Bt is indeed a flux function), but we don't have anything outside the plasma.
        In this program, we fill in the plasma with the field from efit.fpol and to get the field outside the plasma we make the assumption that there is no current outside the plasma, which allows to apply (Rsep/R) factors in order to get the field at point (R,Z), where Rsep is the radius of the separatrix at height Z...
        Please note that the convention is that a positive Bt corresponds to the field going counterclockwise seen from the top, in agreement with the convention for the ERGOS toroidal angle.
    """

    Bt = _np.empty((nZ, nR))

    size0 = len(efit['fpol'])
    xpsi = _np.linspace(0, 1, num=size0)

    Rcentr = 1.5 # Value of Rcentr (radius at which efit.bcentr is taken, in m)? For JET, i_t should be efit.rzero and for MAST i_t should be 1 (if unsure, check under JETDSP or xpad...).

    ZupperX = _np.max(efit['zbbbs']) # Z (in m) of upper X point? (To be read visually on the psi plot. If no upper X point, value of the maximum Z of the plasma.)
    ZlowerX = _np.min(efit['zbbbs']) # Z (in m) of lower X point? (If no lower X point, value of the minimum Z of the plasma.)'


    for j in range(nZ): 
        if zgrid[j] >= ZlowerX and zgrid[j] <= ZupperX:
            # i=0
            i = _np.argmin(_np.abs(Rgrid - 0.15))
            flag = 0 # This flag tells if the point is inside the plasma or not.

            while flag == 0 and i <= nR-2:
                i += 1
                psi_loc = psi_norm[j,i]
                
                if psi_loc <= 1: 
                    flag = 1 

            # For those Z which have no points in the plasma, we use the vacuum field
            if flag == 0:
                Bt[j,:] = efit['bcentr'] * Rcentr / Rgrid[0:nR]

            # For the other Z 
            if flag == 1:
                psi_loc = psi_norm[j,i]
                fpol_loc = efit['fpol'][_np.argmin(_np.abs(xpsi - psi_loc))]
                Bt_loc = fpol_loc/Rgrid[i]

                # Come back to the R smaller than the separatrix radius
                # and calculate the field for these points
                Bt[j,0:i] = fpol_loc / Rgrid[0:i]
                
                # Points inside the plasma
                while flag == 1 and i <= nR-1:
                    psi_loc = psi_norm[j,i]
                    fpol_loc = efit['fpol'][_np.argmin(_np.abs(xpsi - psi_loc))]
                    Bt[j,i] = fpol_loc/Rgrid[i]

                    if psi_loc > 1:
                        flag = 0 
                    i += 1

                i -= 1

                # Calculate the field for the points with R larger than
                # the separatrix radius
                psi_loc = psi_norm[j,i]
                fpol_loc = efit['fpol'][_np.argmin(_np.abs(xpsi - psi_loc))]
                Bt_loc = fpol_loc/Rgrid[i]

                while i <= nR-1:
                    Bt[j,i] = fpol_loc/Rgrid[i]
                    i = i + 1
        else:
            Bt[j,:] = efit['bcentr']*Rcentr/Rgrid[0:nR]

    return Bt


def psi_refining(efit:dict, dR:float=0.0025, dZ:float=0.0025):
    """[summary]

    ChangeLog:
        2008/09/22, Refines artificially the EFIT equilibrium data and saves i_t again as an input for different programs, e.g. poincare_footprint.f and mesh_from_efit.f. This is to allow a better accuracy of the calculations.
    """
    import numpy as np
    from scipy.interpolate import interp2d
    
    gfile = efit['gfile']
    R_min = efit['rmin']
    R_max = efit['rmin'] + efit['xdim']
    Z_min = efit['zmid'] - 0.5*efit['zdim']
    Z_max = efit['zmid'] + 0.5*efit['zdim']
    nR = efit['nw']-1
    nZ = efit['nh']-1

    rgrid, zgrid, psi_norm, psi_sep, psi_maxis = psi_norm_calc(efit, nZ, nR)
    
    RM = efit['rmaxis']
    BM = efit['fpol'][0]/efit['rmaxis']
    rmaxis = efit['rmaxis']
    zmaxis = efit['zmaxis']

    R_refined = np.linspace(R_min, R_max, num=int(1.0/dR)) 
    Z_refined = np.linspace(Z_min, Z_max, num=int(1.0/dZ)) 
    dR, dZ = R_refined[1]-R_refined[0], Z_refined[1]-Z_refined[0] # eliminate rounding error on user-specified dR and dZ.

    # The interp2d function here obeys the 'xy' indexing, that is, Cartesian order
    psi_refined = interp2d(rgrid, zgrid, efit['psirz'], kind='cubic')(R_refined, Z_refined)
    psi_norm_refined = interp2d(rgrid, zgrid, psi_norm, kind='cubic')(R_refined, Z_refined)

    #  figure
    #  contour(R_refined, Z_refined, psi_refined, 100)
    #  colorbar

    rgrid, zgrid = R_refined, Z_refined
    efit['psirz'] = psi_refined
    psi_norm = psi_norm_refined

    size_R, size_Z = len(R_refined), len(Z_refined)
    efit['nw'], efit['nh'] = size_R, size_Z
    nR, nZ = efit['nw']-1, efit['nh']-1

    # Z (in m) of upper X point? (To be read visually on the psi plot. If no upper X point, value of the maximum Z of the plasma.)
    # Z (in m) of lower X point? (If no lower X point, value of the minimum Z of the plasma.)'
    ZupperX, ZlowerX = np.max(efit['zbbbs']), np.min(efit['zbbbs']) 

    refined_equili = {
        "gfile": gfile,
        "R_min": R_min, "R_max": R_max,
        "Z_min": Z_min, "Z_max": Z_max,
        "R_maxis": rmaxis, "Z_maxis": zmaxis,
        "nR": nR, "nZ": nZ,
        'R': rgrid, 'Z': zgrid,
        "refined": True,
        "RM": RM, "BM": BM,
        "psi_sep": psi_sep, "psi_maxis": psi_maxis,
        "Z_lim_up": ZupperX, "Z_lim_down": ZlowerX,
        # Note that the following BR, BZ, Bt are not on the mesh R x Z, R x Z indicate the mesh corner positions, while the BR, BZ, Bt are built on the mesh cell center with shape [nR, nZ]. --- Wenyin mark
        'BR': _BR_calc(efit, nZ, nR, rgrid, zgrid).T,
        'BZ': _BZ_calc(efit, nZ, nR, rgrid, zgrid).T,
        'Bt': _Bt_full_calc(efit, nZ, nR, rgrid, zgrid, psi_norm).T,
        # psi_norm is on the mesh R x Z, you can directly use R x Z mesh.
        'psi_norm': psi_norm
    }
    return refined_equili
    


#######################################################
# Equili Part
#######################################################
def EquiliRead(machine:str, gfile:str):
    return _np.load(PathMachine(machine) / 'equili' / (gfile+'.npz'))
        
def EquiliWrite(equili:dict, machine:str, gfile:str, **kwarg):
    if not 'gfile' in equili.keys():
        raise KeyError("gfile(str) item is necessary.")
    
    _np.savez(PathMachine(machine) / 'equili' / (gfile+'.npz'), **equili)


        
# BR = BR_calc(efit, nZ, nR, Rgrid, zgrid),
# BZ = BZ_calc(efit, nZ, nR, Rgrid, zgrid),
# Bt = Bt_full_calc(efit, nZ, nR, Rgrid, zgrid, psi_norm),
# psi_norm = psi_norm

# For test
if __name__ == "__main__":
    r_coil, coil_info = CoilRead(machine_name='EAST')
