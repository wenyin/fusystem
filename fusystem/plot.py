# The following function `plot_psi`, `efit_q_plot` has not yet been verified
def plot_psi(efit:dict, rgrid, zgrid, psi_norm):
    import numpy as np
    import matplotlib.pyplot as plt

    rgrid = np.linspace(
        efit['rmin'], 
        efit['rmin']+efit['xdim'], 
        efit['nw'])
    zgrid = np.linspace(
        efit['zmid']-0.5*efit['zdim'], 
        efit['zmid']+0.5*efit['zdim'], 
        efit['nh'])
    fig, ax = plt.contourf(rgrid, zgrid, psi_norm, 15)
    
    plt.xlabel('R (m)')
    plt.ylabel('Z (m)')
    plt.axis('equal')
    # plt.set(xlim=(0.2, 1), ylim=(-1.4, -0.6))

def efit_q_plot(efit:dict):
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.interpolate import interp1d
    plt.plot(np.linspace(0, 1, num=efit['nprof']), efit['qpsi'])
    psi_q_eq_2 = interp1d(
        efit['qpsi'], 
        np.linspace(0, 1, efit['nprof']),
        2)

def STET_mesh_plot(r_mesh, z_mesh, fig=None, ax=None):
    import matplotlib.pyplot as plt
    import numpy as np 
    ns, ntheta = r_mesh.shape

    if ax is None:
        fig, ax = plt.subplots()
    
    n_fs = 20
    ns_inc = int(np.fix(ns/n_fs))
    k = 0
    while k*ns_inc < ns:
        ax.plot(r_mesh[k*ns_inc,:], z_mesh[k*ns_inc,:])
        k += 1

    ax.plot(r_mesh[ns-2,:], z_mesh[ns-2,:])

    n_pa = 64
    ntheta_inc = int(np.fix(ntheta/n_pa))
    l = 0
    while l*ntheta_inc < ntheta:
        ax.plot(r_mesh[:-1,l*ntheta_inc], z_mesh[:-1,l*ntheta_inc],'r')
        l += 1
    ax.set_xlabel('R (m)')
    ax.set_ylabel('Z (m)')
    ax.axis('equal')
    return fig, ax
