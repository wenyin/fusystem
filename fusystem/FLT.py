import numpy as _np
from numpy import ndarray as _ndarray
from pathlib import Path as _Path

# TODO: Make the FLT2D & FLT3D one function

def FLT2D(start_points_rzphi:_ndarray, R:_ndarray,Z:_ndarray, BR:_ndarray,BZ:_ndarray):
    import scipy.interpolate as interp
    from scipy.integrate import ode
    flt_num = start_points_rzphi.shape[0]
    
    Rg, Zg = _np.meshgrid(R, Z)
    B_RZ = interp.LinearNDInterpolator(
        _np.column_stack((Rg.T.flatten(),Zg.T.flatten())), 
        _np.column_stack((BR.flatten(),BZ.flatten())))

    t0, t1, dt = 0, 5.0e1, 9.0e-1
    streamline = [_np.zeros((int((t1-t0)/dt)+1, 2)) for i in range(flt_num)]
    for f in range(flt_num):
        streamline[f][0,:]=start_points_rzphi[f,:-1]

        r = ode((lambda t, y: B_RZ(y)[0,:])).set_integrator('dopri5') # Now the RK4(5) method is chosen to solve the ode solution 
        r.set_initial_value(start_points_rzphi[f,:-1], t0)

        for i in range(int((t1-t0)/dt)):
            streamline[f][i+1,:] = r.integrate(r.t+dt)
            if not r.successful():
                print("Field line tracing stuck in trouble. Probably due to being out of computational domain.")
                streamline[f] = streamline[f][:i+1,:]
                break 
            if (streamline[f][i+1,0]<Rg.min() or streamline[f][i+1,0]>Rg.max() or streamline[f][i+1,1]<Zg.min() or streamline[f][i+1,1]>Zg.max()):
                print("Field line tracing out of computational domain.")
                streamline[f] = streamline[f][:i+1,:]
                break
    return streamline

def FLT3D(flt_index, init_p_rzphi, div_path, div_RZ, field_intper, pos=True, neg=False, t=20.0e1, dt=2.5e-2, RZlimit=None, mean_free_path:float=None, diffusion_range:float=None, report_progress:bool=True):
    import warnings
    from scipy.integrate import ode
    from fusystem.thirdparty.intersect import intersection # install this library from the third_party folder
    if mean_free_path is not None: 
        import random
        from scipy.spatial.transform import Rotation

    if (not pos) and (not neg):
        raise RuntimeError("Field line tracing function should trace the line at least in one direction. Turn on pos or neg function args. ")
    if not div_path.contains_point(init_p_rzphi[:-1]):
        raise ValueError(f"The {flt_index} field line trace initializes with a point outside of the divertor.")
    
    def strm_side(direction) -> _ndarray:
        sign = "+" if direction==1 else "-" if direction==-1 else print("Illegal func input. direction should be either 1 or -1.")
        if mean_free_path is None:
            strmline_side = _np.empty((int(t/dt)+3, 3))
        else:
            strmline_side = _np.empty((int(2.5* t/mean_free_path) + int(t/dt), 3))
        strmline_side[0,:] = init_p_rzphi
        
        # Now the RK4(5) method is chosen to solve the ode solution 
        r = ode( (lambda t, y: direction * field_intper(y) ) ).set_integrator('dopri5') 
        r.set_initial_value(init_p_rzphi, t=0.0) # Set the initial point for the flt_index line
        
        if report_progress: print(f"The {flt_index} field line trace {sign} ODE ing...")
        i = 0
        if mean_free_path is not None: free_path = random.expovariate(1.0/mean_free_path)
        while t > r.t:
            if mean_free_path is None:
                strmline_side[i+1,:] = r.integrate(r.t+dt) # Sometimes it raises error that out of bounds, in fact you may need to check whether your FLT init point is suitable 
            else:
                if free_path > dt:
                    strmline_side[i+1,:] = r.integrate(r.t+dt)
                    free_path -= dt
                else: # free_path <= dt
                    strmline_side[i+1,:] = r.integrate(r.t+free_path)
                    free_path = random.expovariate(1.0/mean_free_path)
                    if _np.abs(strmline_side[i+1,1]) < 1e-6 and _np.abs(strmline_side[i+1,2]) < 1e-6:
                        if _np.abs(strmline_side[i+1,0]) < 1e-6: raise ValueError('zero vector')
                        else: perp_vec1 = _np.cross(strmline_side[i+1,:], [0, 1, 0])
                    else: perp_vec1 = _np.cross(strmline_side[i+1,:], [1, 0, 0])
                    perp_vec1 /= _np.linalg.norm(perp_vec1)
                    perp_vec2 = _np.cross( strmline_side[i+1,:], perp_vec1 )
                    perp_vec2 /= _np.linalg.norm(perp_vec2)
                    diff_len, diff_angle = random.uniform(0.0, diffusion_range), random.uniform(0.0, 2*_np.pi)
                    perp_vec = perp_vec1 * _np.cos(diff_angle) + perp_vec2 * _np.sin(diff_angle)
                    perp_vec *= diff_len
                    strmline_side[i+1,:] += perp_vec
                    r.set_initial_value(strmline_side[i+1,:], t=r.t)
            # ODE failed with unknown reason
            if not r.successful():
                print(f"The {flt_index} field line trace {sign} ODE stuck in trouble.")
                strmline_side = strmline_side[:i+1,:]
                return strmline_side
            # Field line has been out of the range of interest, this is unnecessary if the divertor edge check executed
            if RZlimit is not None:
                if (strmline_side[i+1,0]<RZlimit[0] or strmline_side[i+1,0]>RZlimit[1] or strmline_side[i+1,1]<RZlimit[2] or strmline_side[i+1,1]>RZlimit[3]):
                    print(f"The {flt_index} field line trace {sign} ODE out of computational domain.")
                    strmline_side = strmline_side[:i+1,:]
                    return strmline_side
            # Check whether the field line has been out of the range of interest
            if not div_path.contains_point(strmline_side[i+1,:]):
                if i == 0: return strmline_side[:1,:] # it is about the edge of divertor, let it return origin only.
                crosR, crosZ = intersection(strmline_side[i:i+2,0], strmline_side[i:i+2,1], div_RZ[:,0], div_RZ[:,1])
                if len(crosR)==0 or len(crosZ)>1:
                    warnings.warn(f"The {flt_index} field line trace {sign} ODE met the divertor edge with no point or more than one points, {(crosR, crosZ)}, for which the function has not yet been prepared to handle. Check the printed crosR & crosZ and consider picking a better init point or decreasing dt. ")
                    return strmline_side[:i+2,:]
                perc = _np.linalg.norm(strmline_side[i,:-1]-[crosR[0], crosZ[0]]) \
                    / _np.linalg.norm( strmline_side[i+1,:-1]-strmline_side[i,:-1])
                assert(perc>=0 and perc<=1)
                strmline_side[i+1,:] = strmline_side[i,:] + (strmline_side[i+1,:]-strmline_side[i,:])*perc
                if report_progress: print(f"The {flt_index} field line trace {sign} ODE out of divertor edge.")
                strmline_side = strmline_side[:i+2,:]
                return strmline_side
            i += 1
        if report_progress: print(f"The {flt_index} field line trace {sign} ODE finished within finite time...")
        strmline_side = strmline_side[:i+1,:]
        return strmline_side
    
    if pos:
        strmline_one_pos = strm_side(1)
    if neg:
        strmline_one_neg = strm_side(-1)
    # The following code is to make sure the _np.array direction from top to bottom, goes along the local B direction. 
    if pos and not neg:
        return strmline_one_pos
    if not pos and neg:
        strmline_one_neg = strmline_one_neg[::-1,:] 
        return strmline_one_neg
    if pos and neg:
        strmline_one_neg = strmline_one_neg[:0:-1,:]
        return _np.concatenate((strmline_one_neg, strmline_one_pos))


# TEST code:
# from ergospy.measure.FLT import FLT_length
# point_num = 100
# Rc, Zc = 1.8, 0.1
# strmline_rzphi = np.empty((point_num, 3)) 
# strmline_rzphi[:,0] = np.ones((point_num))*Rc
# strmline_rzphi[:,1] = np.ones((point_num))*Zc
# strmline_rzphi[:,2] = np.linspace(0,np.pi/2, point_num, endpoint=True)
# assert(np.abs(FLT_length(strmline_rzphi) - 1/2*np.pi*Rc) < 1e-2*np.pi*Rc)
def FLT_length(strmline_rzphi:_ndarray):
    from fusystem.coord import RZPhi2XYZ
    X,Y,Z = RZPhi2XYZ(strmline_rzphi, merge_return=False)
    segment_len = _np.sqrt((X[1:]-X[:-1])**2 + (Y[1:]-Y[:-1])**2 + (Z[1:]-Z[:-1])**2)
    return _np.sum(segment_len)


def FLT_reach_min_s_inside(strmline_rzphi:_ndarray, S:_ndarray, TET:_ndarray, r_mesh:_ndarray, z_mesh:_ndarray):
    from fusystem.coord import RZ2STET
    return _np.min(RZ2STET(strmline_rzphi[...,0:2], S,TET,r_mesh,z_mesh)[...,0])

# TODO: The equili psi_norm data is very old styled, update it in the future.
def FLT_reach_min_s(strmline_rzphi:_ndarray, folder_equilibrium:_Path):
    from scipy.interpolate import RegularGridInterpolator

    # ergospy.coordinate for each point in 3D
    nR, nZ = _np.loadtxt(_Path(folder_equilibrium) / 'nR_nZ.dat', dtype=_np.uint32)
    Rmin,Rmax, Zmin,Zmax = _np.loadtxt(_Path(folder_equilibrium) / 'R_Z_min_max.dat', dtype=_np.float32)

    R = _np.linspace(Rmin, Rmax, nR) # [Rmin, Rmin+dR, ..., Rmax] totally nR elements.
    Z = _np.linspace(Zmin, Zmax, nZ) # [Zmin, Zmin+dZ, ..., Zmax] totally nZ elements.
    # WARNING: I am very strange why the psi_norm have one addition component for each row.
    psi_norm = _np.loadtxt(_Path(folder_equilibrium) / 'psi_norm.dat', dtype=_np.float32)[:-1,:-1].T
    psi_norm_interp = RegularGridInterpolator((R,Z), psi_norm)
    
    return _np.min( psi_norm_interp(strmline_rzphi[...,0:2]) )

def FLT_endpoint(strmline_rzphi:_ndarray):
    assert(strmline_rzphi.ndim==2)
    endpoint_UP = strmline_rzphi[0 ,:]
    endpoint_DOWN=strmline_rzphi[-1,:]
    if endpoint_UP[1]<endpoint_DOWN[1]:
        endpoint_UP, endpoint_DOWN = endpoint_DOWN, endpoint_UP
    return endpoint_UP, endpoint_DOWN