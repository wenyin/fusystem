from fusystem.file import PathMachine
import ipywidgets as widgets
from IPython.display import display

def xcor():
    btns = widgets.ToggleButtons(
        options=['RZPhi', 'STETPhi'],
        description='x coord sys:',
        disabled=False,
        button_style='', # 'success', 'info', 'warning', 'danger' or ''
        tooltips=['Description of slow', 'Description of regular', 'Description of fast'],
    #     icons=['check'] * 3
    )
    display(btns)
    return btns

def fcor():
    btns = widgets.ToggleButtons(
        options=['RZPhi'],
        description='f coord sys:',
        disabled=False,
        button_style='', # 'success', 'info', 'warning', 'danger' or ''
        tooltips=['Description of slow', 'Description of regular', 'Description of fast'],
    #     icons=['check'] * 3
    )
    display(btns)
    return btns

def HPC_mode():
    btns = widgets.ToggleButtons(
        options=['CPU', 'GPU_MEM_0', 'GPU_MEM_1', 'GPU_MEM_2'],
        description='HPC Mode:',
        disabled=False,
        button_style='', # 'success', 'info', 'warning', 'danger' or ''
        tooltips=['Description of slow', 'Description of regular', 'Description of fast'],
    #     icons=['check'] * 3
    )
    display(btns)
    return btns

def CoilSelectionUI(machine_name, grid_col=4):
    """Offer a convenient UI for users to choose the  coil system and coils.

    Args:
        machine_name (str): which machine to query
        grid_col (int, optional): how many checkbox columns are suitable for UI output. Defaults to 4.

    Raises:
        RuntimeError: [description]

    Returns:
        [type]: [description]
        
    >> import asyncio
    >> coil_sel_task = asyncio.create_task(ergos.IPyUI.CoilSelectionUI(machine_name))

    >> import time
    >> time.sleep(2)
    >> if coil_sel_task.done():
    >>     coilsys_sel_names, coil_sel_names = await coil_sel_task
    >>     print(f'Selected Coil Systems: {coilsys_sel_names}')
    >>     print(f'Selected Coils: {coil_sel_names}')
    >> else:
    >>     raise RuntimeError('Please choose the wanted coils >> before going foreward.')
    """    
    from ipywidgets import GridspecLayout, Checkbox, Layout, Button, HBox, Output
    from IPython.display import display, clear_output
    from .file import CoilRead, CoilsysNames

    button1 = Button(description="Confirm Coil System(s)", layout=Layout(height='auto', width='auto')) 
    button2 = Button(description="Confirm Coils(s)", layout=Layout(height='auto', width='auto')) 
            
    path_machine = PathMachine(machine_name)
    coilsys_names = CoilsysNames(machine_name)

    coilsys_checkbx = [Checkbox(value=False, 
                                description=coilsys, 
                                layout=Layout(height='auto', width='auto')) 
                        for coilsys in coilsys_names]
    grid = GridspecLayout((len(coilsys_names)-1)//grid_col + 1, grid_col)
    for i in range(len(coilsys_names)):
        grid[i//grid_col, i - i//grid_col*grid_col] = coilsys_checkbx[i]
    print(f'Pick the coil system(s) you want to calculate the field components of {machine_name}:')
    display(grid)
    display(button1)
    display(button2)
    # display(HBox([button1,button2]))

    coilsys_sel_names = [] 
    coil_sel_names = {}
    coil_checkbx_es = []

    output1 = Output()
    output2 = Output()
    display(output1)
    display(output2)
    def show_coilsys(b):
        with output1:
            clear_output()
            print("Loading...")
            # Coil system names that are selected
            coilsys_sel_names.clear()
            coil_checkbx_es.clear()
            coilsys_sel_names.extend([box.description for box in coilsys_checkbx if box.value])
            _, coil_info = CoilRead(machine_name, coilsys_sel_names, only_info=True)
            clear_output()
            print("These coil systems have been selected:", coilsys_sel_names)

            coil_names = {coilsys:list(coil_info[coilsys].keys())  for coilsys in coilsys_sel_names}
            coil_sum = sum([len(coil_names[coilsys]) for coilsys in coil_names.keys()])


            coil_grid = GridspecLayout((coil_sum-1)//grid_col + 1, grid_col)
            for coilsys in coilsys_sel_names:
                coil_checkbx_es.extend([Checkbox(value=True, description=coilsys+'/'+coil, layout=Layout(height='auto', width='auto')) for coil in coil_names[coilsys]])
            for i, coil_checkbx in enumerate(coil_checkbx_es):
                coil_grid[i//grid_col, i - i//grid_col*grid_col] = coil_checkbx
            display(coil_grid)

    def show_coils(b):
        with output2:
            clear_output()
            # Coil names that are selected
            coil_sel_names.clear()
            coil_sel_names.update({coilsys:[] for coilsys in coilsys_sel_names})
            for box in coil_checkbx_es:
                if box.value:    
                    coilsys, coil = box.description.split('/')
                    coil_sel_names[coilsys].append(coil) 
            print(f"These coil(s) have been selected {coil_sel_names}\n")
            print("Finished updating the coilsys_sel_names & coil_sel_names. If you want to update the names again, run the code one more time.")
    
    button1.on_click(show_coilsys)
    button2.on_click(show_coils)
    return coilsys_sel_names, coil_sel_names

    

# Choose a flux surface
# i_Q avoid non-monotone q(s) curve

def query_surf_index_Q(Q, i_Q=0) -> int:
    import numpy as np
    q_choice = float(input(f'Safety factor of the surface? ({np.min(Q):.3f} - {np.max(Q):.3f}) '))
    if q_choice > np.max(Q) or q_choice < np.min(Q):
        raise RuntimeWarning("Out of the reasonable q safety factor range.")
    i_surf = np.argmin(np.abs(Q[i_Q:] - q_choice))+i_Q
    return i_surf

def query_surf_index_S(S) -> int:
    import numpy as np
    s_choice = float(input(f's magnetic radius of the surface?  ({np.min(S):.4f} - {np.max(S):.4f}) '))
    if s_choice > np.max(S) or s_choice < np.min(S):
        raise RuntimeWarning("Out of the reasonable s magnetic radius range.")
    i_surf = np.argmin(np.abs(S - s_choice))
    return i_surf

def query_surf_index(Q=None, S=None, i_Q=0) -> int:
    import numpy as np
    assert( Q is not None or S is not None )

    if Q is not None and S is not None:
        if int(input("Choose a B surface by safety factor or s magnetic radius? (1 for q, 0 for s) "))==1: 
            i_surf = query_surf_index_Q(Q, i_Q)
        else:
            i_surf = query_surf_index_S(S)
    elif Q is not None and S is None: 
        i_surf = query_surf_index_Q(Q, i_Q)
    elif Q is None and S is not None: 
        i_surf = query_surf_index_S(S)
    return i_surf

def query_n_har(Q) -> int:
    if Q[0]>0:
        n_har = int(input('n (toroidal mode number)? (should be NEGATIVE because q>0)'))
    else:
        n_har = int(input('n (toroidal mode number)? (should be POSITIVE because q<0)'))
    return n_har
