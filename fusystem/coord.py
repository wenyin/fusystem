"""
.. module:: coordinate 
   :platform: Unix, Windows
   :synopsis: Transform between various coordinate system (GPU acceleration optional).

.. moduleauthor:: Wenyin Wei <wenyin.wei@ipp.ac.cn>
"""


import fusystem.thirdparty.gpuoptional as gpuoptional

def RZPhi2XYZ(rzphi, category:str='location', merge_return:bool=True):
    """Turn RZPhi coordinates to XYZ system.

    Args:
        rzphi (numpy.ndarray): with size [..., 3], the last axis is the R,Z,Phi. 
        category (str, optional): what kind of info is the coordinates for, location or field? Defaults to 'location'. Attention, field value coordinate system is a little different from the one in location. 
        merge_return (bool, optional): whether or not to return a [..., 3] numpy.ndarray. Defaults to True. False would return x, y, z respectively.

    Raises:
        ValueError: [description]

    Returns:
        numpy.ndarray: [..., 3] if merge_return True or ([...], [...], [...]) if merge_return False. 
    """
    xp = gpuoptional.get_array_module(rzphi)

    if category=='location':
        x = rzphi[..., 0] * xp.cos(rzphi[..., 2])
        y = rzphi[..., 0] * xp.sin(rzphi[..., 2])
        z = rzphi[..., 1]
    elif category=='field':
        nPhi = rzphi.shape[2]
        Phi = xp.arange(0, 2*xp.pi, 2*xp.pi/nPhi)
        x = rzphi[..., 0] * xp.cos(Phi[None,None, :]) - rzphi[...,2] * xp.sin(Phi[None,None,:])
        y = rzphi[..., 0] * xp.sin(Phi[None,None, :]) + rzphi[...,2] * xp.cos(Phi[None,None,:])
        z = rzphi[..., 1]
    else: raise ValueError('Choose appropriate category, either coord or field.')

    if merge_return:
        return xp.stack((x, y, z), axis=-1)
    else:
        return x, y, z

def XYZ2RZPhi(xyz, category:str='location', merge_return:bool=True):
    """Turn XYZ coordinates to RZPhi system.

    Args:
        xyz (numpy.ndarray): with size [..., 3], the last axis is the R,Z,Phi. 
        category (str, optional): what kind of info is the coordinates for, location or field? Defaults to 'location'. Attention, `field value` coordinate system is a little different from the one of `location`. 
        merge_return (bool, optional): whether or not to return a [..., 3] numpy.ndarray. Defaults to True. False would return r, z, phi respectively.

    Raises:
        ValueError: [description]

    Returns:
        numpy.ndarray: [..., 3] if merge_return True or ([...], [...], [...]) if merge_return False. 
    """
    xp = gpuoptional.get_array_module(xyz)

    r = xp.sqrt(xp.square(xyz[..., 0]) + xp.square(xyz[..., 1]))
    z = xyz[..., 2]
    # TODO: how to avoid zero division, I have not yet figured out
    if category=='location':
        phi = xp.arctan(xyz[..., 1]/xyz[..., 0]) 
    elif category=='field':
        nPhi = xyz.shape[2]
        Phi = xp.arange(0, 2*xp.pi, 2*xp.pi/nPhi)
        phi = xyz[...,1] * xp.cos(Phi[None,None,:]) + xyz[...,0] * xp.sin(Phi[None,None,:])
    else: raise ValueError('Choose appropriate category, either coord or field.')

    if merge_return: return xp.stack((r, z, phi), axis=-1)
    else: return r, z, phi

def coord_system_change(coord_from:str, coord_to:str, r, merge_return:bool=True):
    """Generic Coordinate System Change Interface

    Args:
        coord_from (str): from what coordianate system 'XYZ', 'RZPhi' or etc.
        coord_to (str): to what coordinate system 'XYZ', 'RZPhi' or etc.
        r (_ndarray): location vector array with shape [..., 3]
        merge_return (bool, optional): whether or not to return as a standalone array with shape [..., 3]. Defaults to True.

    Raises:
        ValueError: [description]
        ValueError: [description]
        ValueError: [description]

    Returns:
        [type]: [description]
    """    
    if coord_from==coord_to: 
        if merge_return: return r
        else: return r[...,0], r[...,1], r[...,2]

    if coord_from=='XYZ':
        if coord_to=='RZPhi': return XYZ2RZPhi(r, merge_return=merge_return)
        else: raise ValueError(f"From {coord_from} to {coord_to} transfrom has not yet been prepared.")
    elif coord_from=='RZPhi':
        if coord_to=='XYZ': return RZPhi2XYZ(r, merge_return=merge_return)
        else: raise ValueError(f"From {coord_from} to {coord_to} transfrom has not yet been prepared.")
    else: raise ValueError(f"From {coord_from} to {coord_to} transfrom has not yet been prepared.")

def coord_mirror(coord:str, r, plane:str):
    r_new = r.copy()
    # let the z components be inverse
    if plane=='xy':
        if coord=='XYZ': r_new[..., 2] *= -1.0 
        elif coord=='RZPhi': r_new[..., 1] *= -1.0
    else:
        raise ValueError(f"Not yet prepared for mirror around the plane {plane}.")
    return r_new
    
# - ds : $S$ neighboring difference
# - dsR : $dR/ds(s,\theta)$ 
# - dsZ : $dZ/ds(s,\theta)$ 
# - dtheta : $\theta$ neighboring difference 
# - dthetaR : $dR/d\theta(s,\theta)$ 
# - dthetaZ : $dZ/d\theta(s,\theta)$
def Jac_RZ2STET(S, TET, r_mesh, z_mesh):
    """Acquire a jacobian matrix from RZ coordiante system to STET

    Args:
        S (numpy.ndarray): S axis array, S.ndim==1, 
        TET (numpy.ndarray): TET axis array, TET.ndim==1,
        r_mesh (numpy.ndarray): R coordinate on the mesh weaved by S, TET.
        z_mesh (numpy.ndarray): Z coordiante on the mesh weaved by S, TET.

    Raises:
        ValueError: [description]
        ValueError: [description]

    Returns:
        (ndarray, ndarray, ndarray, ndarray): dS/dR, dS/dZ, dTET/dR, dTET/dZ, on the mesh weaved by S, TET.
    """    
    xp = gpuoptional.get_array_module(S)

    if not (S.ndim==1 and TET.ndim==1):
        raise ValueError('Input S, TET should has dimension 1.')
    if not (r_mesh.ndim==2 and z_mesh.ndim==2):
        raise ValueError('Input r_mesh, z_mesh should has dimension 1.')

    ds = xp.roll(S, 1) - xp.roll(S,-1)
    ds[0], ds[-1] = S[1]-S[0], S[-1]-S[-2]
    dsR = xp.roll(r_mesh, 1, axis=0) - xp.roll(r_mesh, -1, axis=0)
    dsZ = xp.roll(z_mesh, 1, axis=0) - xp.roll(z_mesh, -1, axis=0)
    dsR[ 0,:], dsZ[ 0,:] = r_mesh[ 1,:]-r_mesh[ 0,:], z_mesh[ 1,:]-z_mesh[ 0,:]
    dsR[-1,:], dsZ[-1,:] = r_mesh[-1,:]-r_mesh[-2,:], z_mesh[-1,:]-z_mesh[-2,:]
    dsR = dsR / ds[:, None]
    dsZ = dsZ / ds[:, None]

    # TODO: Add assert that the theta* is equally-spaced.
    dtheta = xp.roll(TET, 1) - xp.roll(TET,-1)
    dtheta[0], dtheta[-1] = TET[1]-TET[0], TET[-1]-TET[-2]
    dthetaR = xp.roll(r_mesh, 1, axis=1) - xp.roll(r_mesh, -1, axis=1)
    dthetaZ = xp.roll(z_mesh, 1, axis=1) - xp.roll(z_mesh, -1, axis=1)
    dthetaR[:, 0], dthetaZ[:, 0] = r_mesh[:, 1]-r_mesh[:, 0], z_mesh[:, 1]-z_mesh[:, 0]
    dthetaR[:,-1], dthetaZ[:,-1] = r_mesh[:,-1]-r_mesh[:,-2], z_mesh[:,-1]-z_mesh[:,-2]
    dthetaR = dthetaR / dtheta[None,:]
    dthetaZ = dthetaZ / dtheta[None,:]

    det_kl = dsR*dthetaZ - dthetaR*dsZ
    # partial derivations in the "easy" direction: dsr=dr/ds, dsz=dz/ds...

    # loops on the (s,theta) grid indexes k and l
    # for k in range(ns):
    # 	for l in range(ntheta):
        
        # computation of the partial derivated in the interesting direction : drs=ds/dr...
        # A=[dsR dthetaR; dsZ dthetaZ]
        # check the determinant value not to be zero
        # test2(k,l)=det(A)
                            # B=A^(-1)
    dRs    = dthetaZ / det_kl # B(1,1)
    dZs    =-dthetaR / det_kl # B(1,2)
    dRtheta=-dsZ     / det_kl # B(2,1)
    dZtheta= dsR     / det_kl # B(2,2) 
    return dRs, dZs, dRtheta, dZtheta

def RZ2STET(RZ, S, TET, r_mesh, z_mesh, dRZdSTET_mesh=None):
    
    ###### Unsatisfactory Version ######
    # This old version uses the scipy.interpolate.griddata but the speed performance is not satisfactory.

    ###### Release Version ######
    from scipy.linalg import solve
    xp = gpuoptional.get_array_module(RZ)

    RZ_mesh = xp.stack((r_mesh,z_mesh), axis=-1)
    STET = xp.empty_like(RZ)
    dRdS_mesh, dRdTET_mesh, dZdS_mesh, dZdTET_mesh \
        = calc_dRZdSTET_mesh(S, TET, r_mesh, z_mesh) if dRZdSTET_mesh is None \
        else dRZdSTET_mesh
        
    for x in xp.ndindex(RZ.shape[:-1]):
        RZ_mesh_bias = xp.linalg.norm(RZ_mesh - RZ[x][None,None,:], axis=-1)
        rz0_ind = xp.unravel_index(xp.argmin(RZ_mesh_bias, axis=None), RZ_mesh_bias.shape)
        r0,z0 = r_mesh[rz0_ind], z_mesh[rz0_ind]
        dRdTET = dRdTET_mesh[rz0_ind]
        dZdTET = dZdTET_mesh[rz0_ind]
        dRdS = dRdS_mesh[rz0_ind]
        dZdS = dZdS_mesh[rz0_ind]
        ds,dtheta = solve(
            a = xp.array(
            [[dRdS, dRdTET], 
            [dZdS, dZdTET]]),
            b = xp.array(RZ[x] - [r0,z0]))
        STET[x] = [S[rz0_ind[0]]+ds, TET[rz0_ind[1]]+dtheta]

    return STET

def calc_dRZdSTET_mesh(S, TET, r_mesh, z_mesh):
    xp = gpuoptional.get_array_module(S)

    dTET = TET[1]-TET[0] # The code requires that theta is equally spaced. One may cancel the requirement by using the numpy.gradient function.
    dS = S[1]-S[0]
    # 5 points center difference method to calculate the first order derivative 
    dRdTET_mesh = (
        -  xp.roll(r_mesh, -2, axis=1)
        +8*xp.roll(r_mesh, -1, axis=1)
        -8*xp.roll(r_mesh,  1, axis=1)
        +  xp.roll(r_mesh,  2, axis=1)
    ) / (12*dTET)
    dZdTET_mesh = (
        -  xp.roll(z_mesh, -2, axis=1)
        +8*xp.roll(z_mesh, -1, axis=1)
        -8*xp.roll(z_mesh,  1, axis=1)
        +  xp.roll(z_mesh,  2, axis=1)
    ) / (12*dTET)
    dRdS_mesh = (
        -  xp.roll(r_mesh, -2, axis=0)
        +8*xp.roll(r_mesh, -1, axis=0)
        -8*xp.roll(r_mesh,  1, axis=0)
        +  xp.roll(r_mesh,  2, axis=0)
    ) / (12*dS)
    dZdS_mesh = (
        -  xp.roll(z_mesh, -2, axis=0)
        +8*xp.roll(z_mesh, -1, axis=0)
        -8*xp.roll(z_mesh,  1, axis=0)
        +  xp.roll(z_mesh,  2, axis=0)
    ) / (12*dS)
    dRdS_mesh[1, :] = (r_mesh[2, :] - r_mesh[0, :]) / (2*dS)
    dRdS_mesh[-2,:] = (r_mesh[-1,:] - r_mesh[-3,:]) / (2*dS)
    dZdS_mesh[0, :] = (z_mesh[1, :] - z_mesh[0, :]) / (2*dS)
    dZdS_mesh[-1,:] = (z_mesh[1, :] - z_mesh[0, :]) / (2*dS)
    return dRdS_mesh, dRdTET_mesh, dZdS_mesh, dZdTET_mesh

def STET2RZ(STET, S, TET, r_mesh, z_mesh):
    """Turn STET coordinate to RZ with the necessary mesh infomation given by S, TET, r_mesh, z_mesh.

    Args:
        STET (numpy.ndarray): with shape [..., 2] telling the points which need to be transformed to RZ coordinate.
        S (numpy.ndarray): S axis array, S.ndim==1.
        TET (numpy.ndarray): TET axis array, TET.ndim==1.
        r_mesh (numpy.ndarray): R coordinate on the mesh weaved by S, TET.
        z_mesh (numpy.ndarray): Z coordiante on the mesh weaved by S, TET.

    Returns:
        RZ: with shape [..., 2] giving the requested points RZ coordinate.
    """    
    ##### Deprecated Version #####
    # S_mesh = xp.ones_like(r_mesh) * S[:,None]
    # TET_mesh=xp.ones_like(r_mesh)*TET[None,:]
    # points = xp.stack((S_mesh.flatten(), TET_mesh.flatten()), axis=-1)

    # grid_R = griddata(points, r_mesh.flatten(), (STET[...,0], STET[...,1]), method='linear')
    # grid_Z = griddata(points, z_mesh.flatten(), (STET[...,0], STET[...,1]), method='linear')
    from scipy.interpolate import RegularGridInterpolator
    xp = gpuoptional.get_array_module(STET)

    R_interp = RegularGridInterpolator((S,TET), r_mesh)
    Z_interp = RegularGridInterpolator((S,TET), z_mesh)
    grid_R = R_interp(STET)
    grid_Z = Z_interp(STET)
    return xp.stack((grid_R, grid_Z), axis=-1)

# From BR & BZ to B1 & Br which are the radial-like magnetic components perpendicular to the B surface.
def BRZ2B1(BR_Bsurf, BZ_Bsurf, path_equili, ret_Br_Bsurf=False, ret_br_Bsurf=False):
    from scipy import io
    import numpy as xp
    xp = gpuoptional.get_array_module(BR_Bsurf)
    equili_dict = io.loadmat(str(path_equili))

    r_mesh, z_mesh = equili_dict['r_mesh'], equili_dict['z_mesh']
    S, TET = equili_dict['S'][:,0], equili_dict['TET'][:,0]
    dRs, dZs, _,_  = Jac_RZ2STET(S, TET, r_mesh, z_mesh)

    RM, B0, BM = equili_dict['RM'][0,0], equili_dict['B0'][0,0], equili_dict['BM'][0,0]
    b3 = equili_dict['b3']

    B3_Bsurf_pol = 1/(RM/(b3*B0*BM))
    B1_Bsurf = dRs[:,:,None] * BR_Bsurf + dZs[:,:,None] * BZ_Bsurf

    ret_dict = dict()
    ret_dict['B1_Bsurf'] = B1_Bsurf
    if ret_Br_Bsurf: 
        Br_Bsurf = B1_Bsurf / xp.sqrt(xp.square(dRs[:,:,None])+xp.square(dZs[:,:,None]))
        ret_dict['Br_Bsurf'] = Br_Bsurf
    if ret_br_Bsurf: 
        b1_Bsurf = B1_Bsurf / B3_Bsurf_pol[:,:,None]
        ret_dict['b1_Bsurf'] = b1_Bsurf
    return ret_dict


# Though it is sequential process, still fast enough.
def RZmesh_interpolate(R,Z, field_rz, r_mesh, z_mesh):
    from scipy.interpolate import interp2d
    import numpy as xp
    # TODO: Use griddata to replace the interp2 function
    # Simplify the function for complex number case by recursion
    if xp.iscomplexobj(field_rz): 
        return RZmesh_interpolate(R,Z, field_rz.real, r_mesh,z_mesh) \
            + 1j*RZmesh_interpolate(R,Z, field_rz.imag, r_mesh,z_mesh)

    if field_rz.ndim==3:
        third_dim_n = field_rz.shape[-1]
        field_interpolated = xp.empty( [r_mesh.shape[0], r_mesh.shape[1], third_dim_n] )
        for i in range(third_dim_n):
            field_interpolated[:,:,i] = RZmesh_interpolate(R,Z, field_rz[:,:,i], r_mesh,z_mesh)
        return field_interpolated
    elif not field_rz.ndim==2:
        raise ValueError("`field_rz` should have dimension of 2 or 3.")

    '''

    Though it is sequential process, still fast enough.
    (k,l) for indexing over the (s, \theta) mesh

    (1) There is an alternative to use griddata rather than interp2d to interpolate the points. 
    However, the performance is about 4 times faster than the (2) one. The only strangest thing is that the system part of CPU work is extremely high at about 30%.  
      from scipy.interpolate import griddata 
      Rg,Zg = xp.meshgrid(R,Z)
      points = xp.stack((Rg.flatten(),Zg.flatten()), axis=-1)
      field_interpolated = griddata(points, field_rz.T.flatten(), (r_mesh, z_mesh), method='cubic')


    (2) The following code use interp2d instead
      field_interpolated = xp.empty_like(r_mesh)
      ns, ntheta = r_mesh.shape
      field_interp = interp2d(x=R,y=Z, z=field_rz.T, kind='cubic')
      for k in range(ns):
          for l in range(ntheta):
              field_interpolated[k,l] = field_interp(r_mesh[k,l], z_mesh[k,l])

    (3) Now our code use RegularGridInterpolator
    '''
    from scipy.interpolate import RegularGridInterpolator

    interp = RegularGridInterpolator((R,Z), field_rz)
    interped_points = xp.stack((r_mesh.flatten(),z_mesh.flatten()), axis=-1)
    return interp(interped_points).reshape(r_mesh.shape)

def STET_mesh_calc(machine:str, gfile:str, gfile_out:str=None, ns:int=40, ntheta:int=80):
    ################################################################
    # 21/01/09, Eric Nardon
    # Calculates the input data for ERGOS (field aligned coordinates etc.)
    # from an EFIT equilibrium file.
    ################################################################
    import numpy as np
    from fusystem.file import EquiliRead, EquiliWrite
    if not gfile_out: gfile_out = gfile + '_fs'

    efit_data = EquiliRead(machine, gfile)
        
    # TODO: IPywidget interactive manipulation should be added here
    print("Please specify the mesh (ns, ntheta) you want. We use ns = 40, ntheta = 80 as default.")

    R_mesh, Z_mesh = [np.empty((ns, ntheta)) for _ in range(2)]

    # Read EFIT equilibrium files
    nR = efit_data['nR']
    nZ = efit_data['nZ']

    BR, BZ, Bt = [efit_data[key] for key in ['BR', 'BZ', 'Bt']]
    psi_norm = efit_data['psi_norm'].transpose() # psi_norm[i,j], i for R, j for Z
    R, Z = efit_data['R'], efit_data['Z']
    # ax.contourf(R, Z, psi_norm)

    R_min, R_max = efit_data['R_min'], efit_data['R_max']
    Z_min, Z_max = efit_data['Z_min'], efit_data['Z_max']
    R_maxis = efit_data['R_maxis']
    Z_maxis = efit_data['Z_maxis']
    psi_sep   = efit_data['psi_sep']
    psi_maxis = efit_data['psi_maxis']
    # Minimum and maximum Z of the plasma
    Z_lim_down = efit_data['Z_lim_down']
    Z_lim_up   = efit_data['Z_lim_up']

    j_min = int(nZ*(Z_lim_down-Z_min) / (Z_max-Z_min))
    j_max = int(nZ*(Z_lim_up  -Z_min) / (Z_max-Z_min)) + 1

    RM = efit_data['RM']
    BM = efit_data['BM']
    # End of reading EFIT equilibrium files

    def index_interp_eq(R, Z) -> (float, float, float, float):
        """Prepare interpolate functions

        Args:
            R (float): the R coordinate of the point you want to approximate
            Z (float): the Z coordinate of the point you want to approximate

        Returns:
            i_interp: the i index (in R direction) of the point which will be used to interpolate (R, Z) value
            j_interp: the j index (in Z direction) of the point which will be used to interpolate (R, Z) value
            coef1: the i index fraction (in R direction) of the point which will be used to interpolate (R, Z) value
            coef2: the j index fraction (in Z direction) of the point which will be used to interpolate (R, Z) value

        ChangeLog:
            Unknown date, Unknown person wrote it in ERGOS/equilibrium_preparation/mesh_from_EFIT.f file.
            2020/11/18, Wenyin Wei rewrite it in python.

        Assertions:
            Make sure your input is legal by yourself.
            assert(R_min < R and R < R_max)
            assert(Z_min < Z and Z < Z_max)

        """
        # Interpolate the magnetic field in the poloidal plane
        i_interp = int(nR * (R-R_min) / (R_max-R_min))
        j_interp = int(nZ * (Z-Z_min) / (Z_max-Z_min))

        R_up   = R_min + ((i_interp+1)/nR) * (R_max-R_min)
        R_down = R_min + (    i_interp/nR) * (R_max-R_min)

        Z_up   = Z_min + ((j_interp+1)/nZ) * (Z_max-Z_min)
        Z_down = Z_min + (    j_interp/nZ) * (Z_max-Z_min)

        coef1 = (R-R_down) / (R_up-R_down)
        coef2 = (Z-Z_down) / (Z_up-Z_down)
        # assert(Z_down <= Z and Z <= Z_up)
        # assert(R_down <= R and R <= R_up)
        return i_interp, j_interp, coef1, coef2

    def B_interp_eq(Bfield:np.ndarray, i_interp, j_interp, coef1, coef2) -> float:
        # Interpolate the magnetic field in the poloidal plane
        # Bfield(nR+1,nZ+1)
        psi11 = Bfield[i_interp,   j_interp]
        psi12 = Bfield[i_interp+1, j_interp]
        psi21 = Bfield[i_interp,   j_interp+1]
        psi22 = Bfield[i_interp+1, j_interp+1]

        B_interp_eq = (1.-coef1)*(1.-coef2)* psi11 \
                        + coef1 *    coef2 * psi22 \
                    + (1.-coef1)*    coef2 * psi21 \
                        + coef1 *(1.-coef2)* psi12
        return B_interp_eq



    def field_interp_eq(R, i_interp, j_interp, coef1, coef2) -> (float, float):
        # Interpolate the magnetic field in the poloidal plane
        psi11 = psi_norm[i_interp,  j_interp  ]
        psi12 = psi_norm[i_interp+1,j_interp  ]
        psi21 = psi_norm[i_interp,  j_interp+1]
        psi22 = psi_norm[i_interp+1,j_interp+1]

        BR_interp = -(1./R)*(nZ/(Z_max-Z_min))*((1.-coef1)*(psi21-psi11)+coef1*(psi22-psi12))
        BR_interp*= psi_sep-psi_maxis
        BZ_interp = (1./R)*(nR/(R_max-R_min))*((1.-coef2)*(psi12-psi11)+coef2*(psi22-psi21))
        BZ_interp*= psi_sep-psi_maxis
        return BR_interp, BZ_interp

    # Calculate G11=||grad(sqrt(psiN))||^2 (required by ERGOS)
    G11 = np.zeros((nR + 1, nZ + 1))
    dR_psiN = (psi_norm[2:nR+1, 1:nZ]-psi_norm[0:nR-1, 1:nZ]) / ((2./nR)*(R_max-R_min))
    dZ_psiN = (psi_norm[1:nR, 2:nZ+1]-psi_norm[1:nR, 0:nZ-1]) / ((2./nZ)*(Z_max-Z_min))
    G11[1:nR, 1:nZ] = (1./(4.*psi_norm[1:nR, 1:nZ]))*(dR_psiN**2 + dZ_psiN**2)

    n_screen_max = 2*(nR+1) + 2*(nZ+1)

    R_screen   = np.empty((n_screen_max)) 
    Z_screen   = np.empty((n_screen_max))
    angle      = np.empty((n_screen_max))
    BR_eq, BZ_eq, Bt_eq = [np.empty((n_screen_max)) for _ in range(3)]
    theta_star = np.empty((n_screen_max))

    # The parameter iR_min is used for cases where psi(R,Z0) is a non-monotonic
    # function of R for some Z0 at the left of the plasma. This can be
    # the case for MAST eqdsk equilibria.
    # Warning: This part may cause "index out of bounds" error for some cases, probably due to a wrong area outside plasma but marked with psi < 0.
    iR_min = np.empty((nZ + 1), dtype=int)
    for j in range(nZ + 1):
        i = 0
        psi_loc = 0.
        while psi_loc < 1:
            i += 1
            psi_loc = psi_norm[i,j]
        iR_min[j] = i

    # For each flux surface, get the a list of points which are equally 
    # spaced in terms of the theta* angle
    q_arr = np.empty((ns))
    print("R_limit:", R_min, R_max)
    print("Z_limit:", Z_min, Z_max)
    for i_s in range(ns):
        psi_screen = ((i_s+1)/ns)**2
        i_screen = 0

        # Obtain a list of (R,Z) coordinates for points on the flux surface.
        # First step: For each value of Z on the mesh, get the Rint and Rext of the surface.
        for j in range(j_min-1, j_max):
            for i in range(iR_min[j], nR-1):
                psi1 = psi_norm[i,  j]
                psi2 = psi_norm[i+1,j]
                sign1 = np.sign(psi_norm[i,  j] - psi_screen)
                sign2 = np.sign(psi_norm[i+1,j] - psi_screen)
                if sign1 != sign2:
                    R_i      = R_min + (   i /nR) * (R_max-R_min)
                    R_iplus1 = R_min + ((i+1)/nR) * (R_max-R_min)
                    R_interp = R_i + (psi_screen-psi1) / (psi2-psi1) * (R_iplus1-R_i)
                    R_screen[i_screen] = R_interp
                    Z_screen[i_screen] = Z_min + (j/nZ) * (Z_max-Z_min)
                    i_screen += 1

        # Second step: For each value of R on the mesh, get the Zdown and Zup of the surface.
        # We avoid Zs which are above or below the plasma.
        for j in range(j_min-1, j_max):
            for i in range(iR_min[j], nR-1):
                psi1 = psi_norm[i,  j]
                psi2 = psi_norm[i,j+1]
                sign1 = np.sign(psi_norm[i,  j] - psi_screen)
                sign2 = np.sign(psi_norm[i,j+1] - psi_screen)
                if sign1 != sign2:
                    Z_j      = Z_min + (    j/nZ) * (Z_max-Z_min)
                    Z_jplus1 = Z_min + ((j+1)/nZ) * (Z_max-Z_min)
                    Z_interp = Z_j + (psi_screen-psi1) / (psi2-psi1) * (Z_jplus1-Z_j)
                    Z_screen[i_screen] = Z_interp
                    R_screen[i_screen] = R_min + (i/nR) * (R_max-R_min)
                    i_screen += 1

        print(f'Number of points on surface psi_norm={psi_screen:.5f} :', i_screen)
        if i_screen == 0:
            print('No point on surface, refine mesh or reduce ns!')
            return

        # We now have a list of i_screen points which are on the flux surface.
        # We need to sort them according to the poloidal angle.
        angle[:i_screen] = np.arctan2(Z_screen[:i_screen]-Z_maxis, R_screen[:i_screen]-R_maxis)
        angle[angle < 0] += 2 * np.pi # In fact only the first i_screen items need to be added 2pi
        
        RZangle_arr = np.array(
            [(R_screen[i], Z_screen[i], angle[i]) for i in range(i_screen)],
            dtype=[('R', float), ('Z', float), ('angle', float)])
        RZangle_arr.sort(order='angle')
        R_screen[:i_screen] = RZangle_arr['R']
        Z_screen[:i_screen] = RZangle_arr['Z']
        angle[:i_screen] = RZangle_arr['angle']
        # for i0 in range(i_screen):
        #     index_min = i0
        #     angle_min = angle[i0]
        #     for i in range(i0, i_screen): # paralleliable
        #         if angle[i] < angle_min:
        #             index_min = i
        #             angle_min = angle[i]
                    
        #     R_screen_bis = R_screen[i0]
        #     Z_screen_bis = Z_screen[i0]
        #     angle_bis = angle[i0]

        #     R_screen[i0] = R_screen[index_min]
        #     Z_screen[i0] = Z_screen[index_min]
        #     angle[i0] = angle[index_min]

        #     R_screen[index_min] = R_screen_bis
        #     Z_screen[index_min] = Z_screen_bis
        #     angle[index_min] = angle_bis

        # We now have a list of i_screen points which are on the surface
        # and which are sorted according to their poloidal angle.

        # We are now going to calculate theta* on each point.
        # For this, we first need to get the magnetic field on each point.
        for i in range(i_screen):
            R_loc = R_screen[i]
            Z_loc = Z_screen[i]
            i_interp, j_interp, coef1, coef2 = index_interp_eq(R_loc, Z_loc)
            BR_eq[i], BZ_eq[i], Bt_eq[i] = [B_interp_eq(B, i_interp, j_interp, coef1, coef2) for B in [BR,BZ,Bt]]

        # Now we get theta* through an integration in the poloidal direction.
        # We first calculate q.
        q = 0.
        R_loc = R_screen[0]
        Z_loc = Z_screen[0]
        sign_Bpol = np.sign( (R_loc-R_maxis) * BZ_eq[0] - (Z_loc-Z_maxis) * BR_eq[0] )
        for i in range(i_screen):
            R_loc = R_screen[i]
            Bpol = sign_Bpol * (BR_eq[i]**2+BZ_eq[i]**2)**0.5
            iplus1 = i + 1 if i != i_screen-1 else 0
            dlpol = ((R_screen[iplus1]-R_screen[i])**2 \
                   + (Z_screen[iplus1]-Z_screen[i])**2)**0.5
            q += (Bt_eq[i]/(R_loc*Bpol)) * dlpol
        q /= 2*np.pi
        q_arr[i_s] = q

        # Now we calculate theta*
        theta_star_loc = 0.
        for i in range(i_screen):
            R_loc = R_screen[i]
            Bpol = (BR_eq[i]**2 + BZ_eq[i]**2)**0.5
            iplus1 = i + 1 if i != i_screen-1 else 0
            dlpol = ((R_screen[iplus1]-R_screen[i])**2 \
                   + (Z_screen[iplus1]-Z_screen[i])**2)**0.5
            theta_star[i] = theta_star_loc
            theta_star_loc = theta_star_loc + np.abs((Bt_eq[i]/(R_loc*Bpol*q))*dlpol)

        # Now we want to obtain a set of ntheta points on the surface
        # which have angles of theta*=2*np.pi*(i_t-1)/(ntheta-1)
        for i_t in range(ntheta):
            theta_star_loc = 2*np.pi*i_t/(ntheta-1.)
            i_interp = 0
            while theta_star[i_interp] <= theta_star_loc and i_interp < i_screen-1:
                i_interp += 1
            i1 = i_interp - 1
            i2 = i_interp
            R1, Z1 = R_screen[i1], Z_screen[i1]
            R2, Z2 = R_screen[i2], Z_screen[i2]
            coef = (theta_star_loc-theta_star[i1]) / (theta_star[i2]-theta_star[i1])
            R_mesh[i_s,i_t] = (1.-coef)*R1 + coef*R2
            Z_mesh[i_s,i_t] = (1.-coef)*Z1 + coef*Z2
    # End of loop on flux surfaces
    
    # Lastly, we calculate B0 and b3, which are required by ERGOS
    # `fs` is short for flux surface.
    G11_fs = np.empty((ns, ntheta))
    BR_fs, BZ_fs, Bt_fs = [np.empty((ns, ntheta)) for _ in range(3)]
    for i_s in range(ns):
        for i_t in range(ntheta):
            # Calculate the interpolation coefficients
            i_interp, j_interp, coef1, coef2 = index_interp_eq(R_mesh[i_s,i_t], Z_mesh[i_s,i_t])
            # Interpolate the B field
            BR_fs[i_s,i_t], BZ_fs[i_s,i_t] = field_interp_eq(R_loc, i_interp, j_interp, coef1, coef2)
            Bt_fs[i_s,i_t] = B_interp_eq(Bt, i_interp, j_interp, coef1, coef2)
            G11_fs[i_s,i_t] = B_interp_eq(G11, i_interp, j_interp, coef1, coef2)

    Bmod_fs = (BR_fs**2 + BZ_fs**2 + Bt_fs**2)**0.5
    B0_fs = Bmod_fs/BM
    b3_fs = (RM/(BM*B0_fs)) * (Bt_fs/R_mesh)

    efit_data_fs = {
        'gfile': efit_data['gfile'],
        'nS': ns, 'nTET' : ntheta, 
        'S': (np.arange(ns)+1)/ns, 
        'TET' : 2*np.pi*np.arange(ntheta)/(ntheta-1),
        'R_mesh' : R_mesh, 'Z_mesh' : Z_mesh,
        'q' : q_arr, 'ASPI' : (np.max(R_mesh)-np.min(R_mesh)) / (np.max(R_mesh)+np.min(R_mesh)), # ASPI is a scalar
        'G11_fs' : G11_fs,
        'B0_fs' : B0_fs, 'b3_fs' : b3_fs,
        'BR_fs' : BR_fs, 'BZ_fs' : BZ_fs, 'Bt_fs' : Bt_fs,
    }
    EquiliWrite(efit_data_fs, machine, gfile_out)


if __name__ == "__main__":
    import file
    import matplotlib 
    matplotlib.use('tkagg') # Agg backend runs without a display
    import matplotlib.pyplot as plt
    equili_dict = file.EquiliDictRead('EAST', '73999_030400ms.mat')
    S, TET = equili_dict['S'][:,0], equili_dict['TET'][:,0]
    r_mesh,z_mesh = equili_dict['r_mesh'], equili_dict['z_mesh']
    import numpy as xp

    STET = xp.stack((xp.ones((200))*0.7, xp.linspace(0,2*xp.pi,200, endpoint=True)), axis=-1)
    RZ = STET2RZ(STET, S, TET, r_mesh, z_mesh)
    STET = RZ2STET(RZ, S, TET, r_mesh, z_mesh)
    plt.scatter(STET[:,0], STET[:,1], s=0.05)

    STET = xp.stack((xp.ones((200))*0.5, xp.linspace(0,2*xp.pi,200, endpoint=True)), axis=-1)
    RZ = STET2RZ(STET, S, TET, r_mesh, z_mesh)
    STET = RZ2STET(RZ, S, TET, r_mesh, z_mesh)
    plt.scatter(STET[:,0], STET[:,1], s=0.05)

    STET = xp.stack((xp.ones((200))*0.004, xp.linspace(0,2*xp.pi,200, endpoint=True)), axis=-1)
    RZ = STET2RZ(STET, S, TET, r_mesh, z_mesh)
    STET = RZ2STET(RZ, S, TET, r_mesh, z_mesh)
    plt.scatter(STET[:,0], STET[:,1], s=0.05)

    plt.show()