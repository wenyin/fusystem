from numpy import ndarray as _ndarray
import numpy as _np


class Field2D:
    def __init__(self, R:_ndarray,Z:_ndarray, BR:_ndarray, BZ:_ndarray, Bt:_ndarray=None):
        self.R, self.Z = R, Z
        self.BR,self.BZ= BR, BZ
        if Bt is not None: self.Bt = Bt
        
        assert( BR.shape == (len(R), len(Z)) ) # To ensure meshgrid right
        assert( BZ.shape == (len(R), len(Z)) ) 
        assert( _np.all(R[1:] > R[:-1]) and _np.all(Z[1:] > Z[:-1])) # To ensure monotone R, Z 

    def nR(self): return len(self.R)
    def nZ(self): return len(self.Z)
    def Rmin(self):return _np.min( self.R )
    def Rmax(self):return _np.max( self.R )
    def Zmin(self):return _np.min( self.Z )
    def Zmax(self):return _np.max( self.Z )
    def check_mesh_uniform(self) -> bool:
        return _np.allclose( self.R[1:]-self.R[:-1], [self.R[1]-self.R[0]]*(self.nR()-1) ) and _np.allclose( self.Z[1:]-self.Z[:-1], [self.Z[1]-self.Z[0]]*(self.nZ()-1) )
    def dR(self): 
        if self.check_mesh_uniform(): return self.R()[1]-self.R()[0]
        else: raise RuntimeError("The mesh is not uniform.")
    def dZ(self):
        if self.check_mesh_uniform(): return self.Z()[1]-self.Z()[0]
        else: raise RuntimeError("The mesh is not uniform.")
    def dRdZ(self): return (self.dR(), self.dZ())
    def shape(self): return (self.nR(), self.nZ())

    def field_max(self, position:int=None):
        B_pol = _np.sqrt(self.BR**2 + self.BZ**2)
        if position is None:
            return _np.max(B_pol)
        elif position is not None:
            return _np.sort(B_pol, axis=None)[- position]
    def field_avg(self):
        B_pol = _np.sqrt(self.BR**2 + self.BZ**2)
        return _np.average(B_pol)
    def field_mag(self) -> _ndarray:
        try:
            return _np.sqrt(self.BR**2 + self.BZ**2 + self.Bt**2)
        except NameError:
            return _np.sqrt(self.BR**2 + self.BZ**2)

    def __add__(self, other):
        if _np.all(self.R==other.R) and _np.all(self.Z==other.Z): # if the two meshes are same, add up them together.
            return Field2D(self.R,self.Z, self.BR+other.BR, self.BZ+other.BZ)
        else:
            from scipy.interpolate import interp2d
            Rmin,Rmax = _np.max( [self.Rmin(), other.Rmin()] ), _np.min( [self.Rmax(), other.Rmax()] )
            Zmin,Zmax = _np.max( [self.Zmin(), other.Zmin()] ), _np.min( [self.Zmax(), other.Zmax()] )
            # The submesh corner index for self mesh R,Z axis
            Rmin_id,Rmax_id = _np.argmin(_np.abs(self.R-Rmin)), _np.argmin(_np.abs(self.R-Rmax))
            Zmin_id,Zmax_id = _np.argmin(_np.abs(self.Z-Zmin)), _np.argmin(_np.abs(self.Z-Zmax))
            R,Z = self.R[Rmin_id:Rmax_id+1], self.Z[Zmin_id:Zmax_id+1]

            BR_interp = interp2d(x=other.R,y=other.Z, z=other.BR.T, kind='cubic')
            BR_interpolated = BR_interp(R, Z).T
            BZ_interp = interp2d(x=other.R,y=other.Z, z=other.BZ.T, kind='cubic')
            BZ_interpolated = BZ_interp(R, Z).T

            return Field2D(R,Z, self.BR+BR_interpolated, self.BZ+BZ_interpolated)

            
class Field3D:

    # Constructors
    def __init__(self, X1:_ndarray,X2:_ndarray,X3:_ndarray, BR:_ndarray, BZ:_ndarray, Bt:_ndarray, xcor='RZPhi', r_mesh:_ndarray=None, z_mesh:_ndarray=None):

        self.xcor = xcor
        self.X1, self.X2, self.X3 = X1, X2, X3 # These could be 'RZPhi', 'RZN', 'STETPhi', 'SMPhi', 'SMN'
        self.BR, self.BZ, self.Bt= BR,BZ,Bt
        if 'S' in xcor:
            self.r_mesh, self.z_mesh = r_mesh, z_mesh
            
        for B_comp in [BR, BZ, Bt]:
            assert( B_comp.shape == (len(X1), len(X2), len(X3)) ) # To ensure meshgrid right

        # To ensure monotone R, Z or S, TET, Phi
        assert(_np.all(self.X1[1:] > self.X1[:-1])) 
        if 'M' not in xcor: assert(_np.all(self.X2[1:] > self.X2[:-1]))
        if 'N' not in xcor: assert(_np.all(self.X3[1:] > self.X3[:-1]))


    # For xcor == 'RZPhi'
    def R(self) -> _ndarray: assert ('R' in self.xcor); return self.X1
    def Z(self) -> _ndarray: assert ('Z' in self.xcor); return self.X2
    def Phi(self) -> _ndarray: assert ('Phi' in self.xcor); return self.X3
    def nR(self) -> int: assert ('R' in self.xcor); return len(self.X1)
    def nZ(self) -> int: assert ('Z' in self.xcor); return len(self.X2)
    def nPhi(self) -> int: assert ('Phi' in self.xcor); return len(self.X3)
    def Rmin(self) -> float: assert ('R' in self.xcor); return _np.min( self.X1 )
    def Rmax(self) -> float: assert ('R' in self.xcor); return _np.max( self.X1 )
    def Zmin(self) -> float: assert ('Z' in self.xcor); return _np.min( self.X2 )
    def Zmax(self) -> float: assert ('Z' in self.xcor); return _np.max( self.X2 )
    def check_mesh_uniform(self) -> bool:
        return _np.allclose( self.R()[1:]-self.R()[:-1], [self.R()[1]-self.R()[0]]*(self.nR()-1) ) and _np.allclose( self.Z()[1:]-self.Z()[:-1], [self.Z()[1]-self.Z()[0]]*(self.nZ()-1) )
    def dR(self): 
        if self.check_mesh_uniform(): return self.R()[1]-self.R()[0]
        else: raise RuntimeError("The mesh is not uniform.")
    def dZ(self):
        if self.check_mesh_uniform(): return self.Z()[1]-self.Z()[0]
        else: raise RuntimeError("The mesh is not uniform.")
    def dRdZ(self): assert ('R' in self.xcor and 'Z' in self.xcor); return (self.dR(), self.dZ())
    def shape(self): assert ('R' in self.xcor and 'Z' in self.xcor); return (self.nR(), self.nZ())

    # For xcor == 'STETPhi'
    def S(self) -> _ndarray: assert ('S' in self.xcor); return self.X1
    def TET(self) -> _ndarray: assert ('TET' in self.xcor); return self.X2
    def M(self) -> _ndarray: assert ('M' in self.xcor); return self.X2
    def N(self) -> _ndarray: assert ('N' in self.xcor); return self.X3
    def nTET(self) -> int: assert ('TET' in self.xcor); return len(self.X2)
    def nN(self) -> int: assert ('N' in self.xcor); return len(self.X3)
    def nM(self) -> int: assert ('M' in self.xcor); return len(self.X2)
    

    def field_max(self, position:int=None):
        B_mag = _np.sqrt(self.BR**2 + self.BZ**2 + self.Bt**2)
        if position is None:
            return _np.max(B_mag)
        elif position is not None:
            return _np.sort(B_mag, axis=None)[- position]
    def field_avg(self):
        B_mag = _np.sqrt(self.BR**2 + self.BZ**2 + self.Bt**2)
        return _np.average(B_mag)
    def field_mag(self):
        return _np.sqrt(self.BR**2 + self.BZ**2 + self.Bt**2)

    def __add__(self, other):
        if isinstance(other, Field2D):
            # Now only for Field2D object with xcor=RZPhi
            if _np.all(self.R()==other.R) and _np.all(self.Z()==other.Z): # if the two meshes are same, add up them together.
                return Field3D(self.R(),self.Z(),self.Phi(), 
                    self.BR+other.BR[:,:,None], 
                    self.BZ+other.BZ[:,:,None], 
                    self.Bt+other.Bt[:,:,None])
            else:
                from scipy.interpolate import interp2d
                Rmin,Rmax = _np.max( [self.Rmin(), other.Rmin()] ), _np.min( [self.Rmax(), other.Rmax()] )
                Zmin,Zmax = _np.max( [self.Zmin(), other.Zmin()] ), _np.min( [self.Zmax(), other.Zmax()] )
                # The submesh corner index for self mesh R,Z axis
                Rmin_id,Rmax_id = _np.argmin(_np.abs(self.R()-Rmin)), _np.argmin(_np.abs(self.R()-Rmax))
                Zmin_id,Zmax_id = _np.argmin(_np.abs(self.Z()-Zmin)), _np.argmin(_np.abs(self.Z()-Zmax))
                R_end,Z_end = self.R()[Rmin_id:Rmax_id+1], self.Z()[Zmin_id:Zmax_id+1]
                BR_end = self.BR[Rmin_id:Rmax_id+1,Zmin_id:Zmax_id+1,:].copy()
                BZ_end = self.BZ[Rmin_id:Rmax_id+1,Zmin_id:Zmax_id+1,:].copy()
                Bt_end=self.Bt[Rmin_id:Rmax_id+1,Zmin_id:Zmax_id+1,:].copy()

                BR_interp = interp2d(x=other.R,y=other.Z, z=other.BR.T, kind='cubic')
                BZ_interp = interp2d(x=other.R,y=other.Z, z=other.BZ.T, kind='cubic')
                Bt_interp = interp2d(x=other.R,y=other.Z, z=other.Bt.T, kind='cubic')
                BR_interpolated = BR_interp(R_end,Z_end).T
                BZ_interpolated = BZ_interp(R_end,Z_end).T
                Bt_interpolated = Bt_interp(R_end,Z_end).T
                for i in range(self.nPhi()):
                    BR_end[...,i] += BR_interpolated
                    BZ_end[...,i] += BZ_interpolated
                    Bt_end[...,i] += Bt_interpolated

                return Field3D(R_end,Z_end,self.Phi(), BR_end, BZ_end, Bt_end)
        else: # Field3D + Field3D
            # First of all, check that two objects have the same xcor
            assert (self.xcor==other.xcor)

            if _np.all(self.X1==other.X1) and _np.all(self.X2==other.X2) and _np.all(self.X3==other.X3): # if the two meshes are same, add up them together.
                if 'S' not in self.xcor:
                    return Field3D(self.X1,self.X2,self.X3, 
                        self.BR+other.BR, 
                        self.BZ+other.BZ, 
                        self.Bt+other.Bt, xcor=self.xcor)
                else:
                    return Field3D(self.X1,self.X2,self.X3, 
                        self.BR+other.BR, 
                        self.BZ+other.BZ, 
                        self.Bt+other.Bt, xcor=self.xcor, r_mesh=self.r_mesh, z_mesh=self.z_mesh)
            else:
                assert _np.all(self.Phi()==other.Phi()) # The interpolation now is still restricted in the 2D poloidal plane
                from scipy.interpolate import interp2d
                Rmin,Rmax = _np.max( [self.Rmin(), other.Rmin()] ), _np.min( [self.Rmax(), other.Rmax()] )
                Zmin,Zmax = _np.max( [self.Zmin(), other.Zmin()] ), _np.min( [self.Zmax(), other.Zmax()] )
                # The submesh corner index for self mesh R,Z axis
                Rmin_id,Rmax_id = _np.argmin(_np.abs(self.R()-Rmin)), _np.argmin(_np.abs(self.R()-Rmax))
                Zmin_id,Zmax_id = _np.argmin(_np.abs(self.Z()-Zmin)), _np.argmin(_np.abs(self.Z()-Zmax))
                R_end,Z_end = self.R()[Rmin_id:Rmax_id+1], self.Z()[Zmin_id:Zmax_id+1]
                BR_end = self.BR[Rmin_id:Rmax_id+1,Zmin_id:Zmax_id+1,:].copy()
                BZ_end = self.BZ[Rmin_id:Rmax_id+1,Zmin_id:Zmax_id+1,:].copy()
                Bt_end = self.Bt[Rmin_id:Rmax_id+1,Zmin_id:Zmax_id+1,:].copy()

                for i in range(len(self.Phi)):
                    BR_interp = interp2d(x=other.R(),y=other.Z(), z=other.BR[...,i].T, kind='cubic')
                    BR_end[...,i] += BR_interp(R_end,Z_end).T
                    BZ_interp = interp2d(x=other.R(),y=other.Z(), z=other.BZ[...,i].T, kind='cubic')
                    BZ_end[...,i] += BZ_interp(R_end,Z_end).T
                    Bt_interp = interp2d(x=other.R(),y=other.Z(), z=other.Bt[...,i].T, kind='cubic')
                    Bt_end[...,i] += Bt_interp(R_end,Z_end).T

                return Field3D(R_end,Z_end,self.Phi(), BR_end, BZ_end, Bt_end)
                

    def __mul__(self, other):
        if isinstance(other, int) or isinstance(other, float):
            BR = self.BR * other
            BZ = self.BZ * other
            Bt=self.Bt * other
        else:
            raise ValueError("Please multiply Field3D object with a scalar.")

        if 'S' not in self.xcor:
            return Field3D(
                self.X1, self.X2, self.X3, 
                BR, BZ, Bt, xcor=self.xcor)
        else:
            return Field3D(
                self.X1, self.X2, self.X3, 
                BR, BZ, Bt, xcor=self.xcor, r_mesh=self.r_mesh, z_mesh=self.z_mesh)

    def zeros_like(self):
        if 'S' not in self.xcor:
            return Field3D(
                self.X1, self.X2, self.X3, 
                _np.zeros_like(self.BR), 
                _np.zeros_like(self.BZ), 
                _np.zeros_like(self.Bt), xcor=self.xcor)
        else:
            return Field3D(
                self.X1, self.X2, self.X3, 
                _np.zeros_like(self.BR), 
                _np.zeros_like(self.BZ), 
                _np.zeros_like(self.Bt), r_mesh=self.r_mesh, z_mesh=self.z_mesh)


    def empty_like(self):
        if 'S' not in self.xcor:
            return Field3D(
                self.X1, self.X2, self.X3, 
                _np.empty_like(self.BR), 
                _np.empty_like(self.BZ), 
                _np.empty_like(self.Bt), xcor=self.xcor)
        else:
            return Field3D(
                self.X1, self.X2, self.X3, 
                _np.empty_like(self.BR), 
                _np.empty_like(self.BZ), 
                _np.empty_like(self.Bt), r_mesh=self.r_mesh, z_mesh=self.z_mesh)

    def xcor_RZ2STET(self, S:_ndarray, TET:_ndarray, r_mesh:_ndarray, z_mesh:_ndarray):
        assert ('R' in self.xcor and 'Z' in self.xcor)
        from scipy import interpolate
        from fusystem.coord import RZmesh_interpolate

        B_comp_STET = lambda B_comp: RZmesh_interpolate(
            self.R(), self.Z(),
            B_comp, r_mesh, z_mesh) 
        return Field3D(S,TET,self.Phi(), 
            BR=B_comp_STET(self.BR),
            BZ=B_comp_STET(self.BZ),
            Bt=B_comp_STET(self.Bt),
            xcor='STETPhi', r_mesh=r_mesh,z_mesh=z_mesh)
            
    
    def fourier_Phi2N(self):
        assert ('Phi' in self.xcor)
        n_max = min(20, self.nPhi()//3)
        n_range = list(range(n_max+1)) + list(range(-n_max,0,1))

        if self.xcor=='RZPhi': to_xcor = 'RZN'; r_mesh, z_mesh = None, None
        elif self.xcor=='STETPhi':to_xcor = 'STETN'; r_mesh, z_mesh = self.r_mesh, self.z_mesh
        else: raise ValueError('For which components of the field you want to do the Fourier transform on phi space?')
        
        return Field3D(
             self.X1, self.X2, n_range, 
            (_np.fft.fft(self.BR, axis=2) / self.nPhi())[:,:,n_range],
            (_np.fft.fft(self.BZ, axis=2) / self.nPhi())[:,:,n_range],
            (_np.fft.fft(self.Bt, axis=2) / self.nPhi())[:,:,n_range],
            xcor=to_xcor, r_mesh=r_mesh, z_mesh=z_mesh)
            

    def fourier_TET2M(self):
        assert ('TET' in self.xcor)
        m_max = min(50, self.nTET()//3)
        m_range = list(range(m_max+1)) + list(range(-m_max,0,1))
        
        if self.xcor=='STETPhi' : to_xcor = 'SMPhi'
        elif self.xcor=='STETN' : to_xcor = 'SMN'
        else: raise ValueError("Not yet implemented for other xcor than STETPhi and STETN.")

        return Field3D(
            self.X1, m_range, self.X3,
            (_np.fft.fft(self.BR[:,:-1,:], axis=1) / (self.nTET()-1))[:,m_range,:],
            (_np.fft.fft(self.BZ[:,:-1,:], axis=1) / (self.nTET()-1))[:,m_range,:],
            (_np.fft.fft(self.Bt[:,:-1,:], axis=1) / (self.nTET()-1))[:,m_range,:],
            xcor=to_xcor, r_mesh=self.r_mesh, z_mesh=self.z_mesh)

    
    # For the sake of raidal-like components and its spectrum. 
    def get_B1(self):
        assert (self.xcor == 'STETPhi')
        from fusystem.coord import Jac_RZ2STET
        dRs, dZs, _, _ = Jac_RZ2STET(self.S(), self.TET(), self.r_mesh, self.z_mesh)
        B1 = dRs[:,:,None]*self.BR + dZs[:,:,None]*self.BZ
        return B1
    def get_B1_n(self):
        if self.xcor=='STETPhi': return self.fourier_Phi2N().get_B1_n()
        elif self.xcor=='STETN': pass
        else: raise ValueError('get_B1_n is for Field3D with xcor STET(Phi/N). If you have a Field3D object with xcor RZPhi, turn it to STETPhi by xcor_RZ2STET method.')
        from fusystem.coord import Jac_RZ2STET
        dRs, dZs, _, _ = Jac_RZ2STET(self.S(), self.TET(), self.r_mesh, self.z_mesh)
        B1_n = dRs[:,:,None]*self.BR + dZs[:,:,None]*self.BZ
        return B1_n

    # \tilde{b}^r = B^1/B_0 (B_0, vacuum toroidal magnetic field on the geometrical axis)
    def get_br(self, BM, RM, G11:_ndarray):
        br = self.get_B1()/(BM * _np.sqrt(G11)/RM)[:,:,None]
        return br
    def get_br_n(self, BM, RM, G11:_ndarray):
        br_n = self.get_B1_n()/(BM * _np.sqrt(G11)/RM)[:,:,None]
        return br_n
    def get_br_mn(self, BM, RM, G11:_ndarray):
        br_mn = _np.fft.fft(self.get_br_n(BM, RM, G11)[:,:-1,:], axis=1) / (self.nTET()-1)
        return br_mn
    
    # \tilde{b}^1 = B^1/B^3
    def get_tilde_b1(self, BM, RM, b3:_ndarray, B0:_ndarray): 
        tilde_b1 = self.get_B1() / (b3 * B0 *BM / RM)[:,:,None]
        return tilde_b1
    def get_tilde_b1_n(self, BM, RM, b3:_ndarray, B0:_ndarray): 
        tilde_b1_n = self.get_B1_n() / (b3 * B0 *BM / RM)[:,:,None]
        return tilde_b1_n
    def get_tilde_b1_mn(self, BM, RM, b3:_ndarray, B0:_ndarray):
        tilde_b1_mn = _np.fft.fft(self.get_tilde_b1_n(BM, RM, b3, B0)[:,:-1,:], axis=1) / (self.nTET()-1)
        return tilde_b1_mn