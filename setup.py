import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="fusystem", 
    version="0.0.2",
    author="Wenyin Wei",
    author_email="wenyin.wei.ww@gmail.com",
    description="Fusion plasma fundamental infrastructure",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/wenyin/fusystem",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        # "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)