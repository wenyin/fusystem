# Python Field Module
This is a module for handling field needed by ERGOS, written by Wenyin@TsinghuaU.

## For Developers
For the newcoming developers, please follow the [How should I perform imports in a python module without polluting its namespace?](https://stackoverflow.com/questions/7423744/how-should-i-perform-imports-in-a-python-module-without-polluting-its-namespace) to avoid your module polluting others' namespace. It is suggested to `import module as __module` or import the modules inside your function to restrict their scope. Which way do you need to use in fact depends on your module size.



