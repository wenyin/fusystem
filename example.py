import numpy as np
import matplotlib.pyplot as plt
# import fusystem
import fusystem.file as fufile
import fusystem.coord as fucoord
import fusystem.plot as fuplot

from pathlib import Path

p_data = fufile.SetDataPath(Path.cwd() / 'data')
# p_data = Path.cwd() / 'data'
machine, gfile = 'EAST', 'g073999.030400'
efit = fufile.read_geqdsk(p_data / machine / 'equili' / gfile)
# Refine the mesh acquired from the EFIT g-file, producing `.json` and `_field_xRZ.npz` file to store scalar parameter and array data respectively.
refined_equili = fufile.psi_refining(efit)
fufile.EquiliWrite(refined_equili, machine, gfile)

# Load the data we just saved.
field_xRZ = fufile.EquiliRead(machine, gfile)
R,Z, psi_norm = field_xRZ['R'], field_xRZ['Z'], field_xRZ['psi_norm']

# Draw psi_norm distribution
fig, ax = plt.subplots()
psi_norm_contour = ax.contour(R, Z, psi_norm, levels=np.arange(1,16)/10)
ax.contourf(R, Z, psi_norm)
ax.clabel(psi_norm_contour, inline=True, fontsize=8)
ax.set_xlabel('R (m)')
ax.set_ylabel('Z (m)')
ax.axis('equal')

# Calculate teh (s, theta*) mesh based on the psi_norm distribution
fucoord.STET_mesh_calc(machine, gfile, ns=40, ntheta=80)
field_STET = fufile.EquiliRead(machine, gfile+'_fs')
# Draw (s, theta*) mesh on the ax, by a mesh, we mean the mesh line is either s-equal or theta*-equal.
fuplot.STET_mesh_plot(
    field_STET['R_mesh'],
    field_STET['Z_mesh'], fig, ax
)

plt.show()

